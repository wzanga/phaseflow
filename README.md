# [Statistical physics] Phase Space Conservative Transport

This library implements a solver for the phase-space Liouville conservation equation :

```math
\frac{\partial f}{\partial t} + \nabla \cdot( u f)  = 0
```

where $`f = f(x,t)`$ is a real-valued distribution function that is advected along the natural trajectories defined by the velocity field  $`u = u(x,t) \in \mathbf{R}^{n}`$.
In general $`t \geq 0`$ represents time and $`x = (x_0,x_1, \dots, x_{n-1}) \in \mathbf{R}^{n}`$ is a phase-state.
For a $`d`$-dimensional newtonian dynamical system with position $`r`$ and velocity $`v`$, we have $`x = (r,v) \in \mathbf{R}^{2d}`$ and the velocity field is
$`u(x,t) = (v, \dot{v})`$ with $`\dot{v}=\text{acceleration} = \text{force/mass}`$. The analytical solution is approximated by a Semi-Lagrangian Particles Method.

**Bibliography** :

* *High order Semi-Lagrangian particle methods for transport equations: numerical analysis and implementation issues*,
Georges-Henri Cottet, Jean-Matthieu Etancelin, Franck Pérignon, Christophe Picard
* *Noiseless Vlasov-Poisson simulations with polynomially transformed particles*, Martin Campos Pinto (CNRS & LJLL, Paris)
* *Towards smooth particle methods without smoothing*, Martin Campos Pinto
* *A high-order accurate Particle-In-Cell method for Vlasov-Poisson problems over long time integrations*, Andrew Myers

## Example 1: pendulum
The state of a pendulum is described by $`\bm{x} = (x,v) \in \mathbf{R}^{2}`$  and the the velocity field
$`u(\bm{x},t) = (v, - \omega^2 \sin x)`$. The figures below displays the evolution of $`f(\bm{x},0)`$ as a 2D Hump density and 100 Monte Carlo points *(blue)*.

<img align="center"  src=examples/data/pendulum.gif width="650">


## Example 2: swirling
The figure below displays the solution to a 2D swirling motion applied to a Zalesak notched disk. The phase state is described with More explicitly, the velocity field is  $`\bm{x} = (x,y)`$ and the velocity field is 
$`u(\bm{x},t) = \cos(\frac{\pi t}{T})\cdot [\sin(\frac{\pi}{2}  x)^2\cdot sin(\pi y),  \sin(\frac{\pi}{2}  y)^2\cdot sin(\pi x)]`$.

<img align="center"  src=examples/data/swirling.gif width="650">


## Minimum configuration
* Languages: *Python>=3.7.7, Cython (used to translate heavy functions into C or C++)*
* OpenMP: *used for multi-core distribution of GIL-free code*
* Other requirements: *check the requirements.txt file for python modules*

## Compiling and running and cleaning
* Compiling : To compile the code, issue the command *make compile*
* Running : To run an example (e.g pendulum ) issue the command *python -m examples.pendulum*
* Cleaning : To clean C/C++ object created by the Cython compiler, issue the command *make clean*
