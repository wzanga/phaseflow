# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native -fopenmp
# distutils: extra_link_args = -lomp
# COMPILER=CC=/usr/local/Cellar/llvm/10.0.1_1/bin/clang++

compile:
	python setup.py build_ext --inplace

log:
	git log --graph --decorate --pretty=oneline --abbrev-commit

reset:
	git reset --hard origin/dev

clean:
	find . -name '*.c' | xargs rm -f
	find . -name '*.cpp' | xargs rm -f
	find . -name '*.so' | xargs rm -f
	find . -name '*.pyc' | xargs rm -f
	find . -name '*.npy' | xargs rm -f
	rm -rf build

stage:
	find . -name '*.pxd' | xargs git add
	find . -name '*.pyx' | xargs git add
	find . -name '*.py' | xargs git add
	find . -name '*.gif' | xargs git add
	find . -name '*.h' | xargs git add
	git add README.md
	git add Makefile
	git add requirements.txt
	git status

test:
	make compile && python -m core.tests.runner -v

vdp:
	make compile && python -m examples.vanderpol

pen:
	make compile && python -m examples.pendulum

par:
	make compile && python -m examples.parachute

swi:
	make compile && python -m examples.swirling

lkv:
	make compile && python -m examples.lotkavolterra

kepler:
	make compile && python -m examples.kepler2BP

lorenz:
	make compile && python -m examples.lorenz

sir:
	make compile && python -m examples.SIR
