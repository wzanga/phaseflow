#------------------------------------------------------------------------------#
# Forward Semi-Lagrangian Method Particles solver for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.solvers.LPM.LPM cimport LPM
from core.commons.kernels.kernel cimport Kernel
from core.commons.distributions.distribution cimport Distribution
from core.commons.flows.flow cimport Flow

cdef class LPM2D(LPM):
    pass
