#------------------------------------------------------------------------------#
# Lagrangian Particles Method absctract solver for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native
from core.solvers.LPM.LPM2D.LPM2D cimport LPM2D
from core.commons.kernels.kernel cimport Kernel
from core.commons.distributions.distribution cimport Distribution
from core.commons.flows.flow cimport Flow
from cython.parallel import prange
from cython.view cimport array as cvarray
from core.commons.maths.sparsematrix cimport SparseMatrix


cdef class LPM2DTensorial(LPM2D):

  def __cinit__(self, Flow flow, Kernel kernel, Distribution dist, double[:,::1] S, unsigned int Nr):
    """
      flow      : c object    : mathematical flow
      kernel    : c object    : interpolation and remapping kernel
      dist      : c object    : initial distribution
      spacetime : double[][3] : phase space-time axes
      Nr        : integer     : number of mass smoothing/redistribution for positiveness
    """
    self.splitting_stages = 1
    self.splitting_scalers = [1]
    self.splitting_directions = [-1]


  cdef void compute_sparse_entries(self, unsigned long col, unsigned short direction, SparseMatrix matrix) nogil:
    """ Compute the coefficients (row, col, weights) of the sparse transition table
        Inputs:
          col       : integer : column index
          direction : integer : splitting direction
    """
    cdef :
        bint flow_stationary = self.flow.is_stationary()
        int[:,::1] bounds
        double[::1] point
        unsigned int row, i
        double w
        int[2] I
    with gil:
        bounds = cvarray(shape=(self.Nd,2), itemsize=sizeof(int), format="i")
        point = cvarray(shape=(self.Nd,), itemsize=sizeof(double), format="d")
    self.get_influence_zone(col, bounds)

    for I[0] in range(bounds[0,0], bounds[0,1]):
        for I[1] in range(bounds[1,0], bounds[1,1]):
            self.get_point_with_indices(I, point)
            w = self.P0DM[col, self.Nd]
            for i in range(self.Nd): w *= self.kernel.evaluate( (point[i] - self.P0DM[col,i])/self.spacetime[i,2] )
            if w!=0:
                row = self.encode(I)
                if flow_stationary: matrix.insert(row, col, w)
                else: self.f[row] += w * self.strengths[col]
