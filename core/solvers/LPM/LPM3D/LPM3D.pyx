#------------------------------------------------------------------------------#
# Forward Semi-Lagrangian Method Particles solver for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native
from cython.operator import dereference, postincrement
from libcpp.map cimport map as cmap
from libc.math cimport fabs
import numpy as np


cdef class LPM3D:

  def __init__(self, Flow flow, Kernel kernel, Distribution dist, double[:,::1] S, unsigned int Nr):
      """
        flow      : c object    : mathematical flow
        kernel    : c object    : interpolation and remapping kernel
        dist      : c object    : initial distribution
        spacetime : double[][3] : phase space-time axes
        Nr        : integer     : number of mass smoothing/redistribution for positiveness
      """
      super().__init__(flow, kernel, dist, S, Nr)


  def get_distribution(self):
      """ return the distribution at the current time
        	Output :
            f(X,t)  : double Nd array : distribution as a numpy array
      """
      return np.asarray(self.f, dtype=np.float64).reshape(self.dims[0],self.dims[1], self.dims[2])


  cdef void regularize_distribution(self):
      """ Correct the (over/under)shoots with mass redistribution based upon
          a local or global a flux-corrected transport algorithm
      """
      cdef:
          double delta
          double capacity
          cmap[int,double] neighboors
          cmap[int,double].iterator it
          double min_value = self.dist.get_min()
          double max_value = self.dist.get_max()
          unsigned short s = self.kernel.get_radius()
          unsigned long i
          unsigned long j
          int[3] I
          int[3] J
          int[3] K

      for i in range(self.Np):
          if (self.f[i]<min_value or self.f[i]>max_value) and (fabs(self.f[i])>=self.threshold):
              #determinate the amount of (over/under)shoots
              delta = self.f[i] - (min_value if self.f[i]<min_value else max_value)
              capacity = 0
              neighboors.clear()
              self.decode(i,I)
              #select the neighboors than can absorb the (over/under)shoots
              for K[0] in range(2*s):
                  J[0] = K[0] + I[0]-s+1
                  if J[0]<0 or J[0]>=self.dims[0] : continue
                  for K[1] in range(2*s):
                      J[1] = K[1] + I[1]-s+1
                      if J[1]<0 or J[1]>=self.dims[1] : continue
                      for K[2] in range(2*s):
                          if J[2]<0 or J[2]>=self.dims[2] : continue
                          j = self.encode(J)
                          if delta<0 and self.f[j]>min_value :
                              neighboors[j] = self.f[j] - min_value
                              capacity += neighboors[j]
                          elif delta>0 and self.f[j]<max_value :
                              neighboors[j] = max_value - self.f[j]
                              capacity += neighboors[j]
              #mass redistribution if the neighboorhood has enough capacity
              if capacity > fabs(delta):
                  it = neighboors.begin()
                  while(it != neighboors.end()):
                      self.f[ dereference(it).first ] += dereference(it).second * delta / capacity
                      postincrement(it)
                  self.f[i] -= delta
