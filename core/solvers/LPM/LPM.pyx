#------------------------------------------------------------------------------#
# Lagrangian Particles Method absctract solver for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native
from cython.parallel import prange
from cython.view cimport array as cvarray
from libc.math cimport fmin, fmax
import tqdm


cdef class LPM:

    def __init__(self, Flow flow, Kernel kernel, Distribution dist, double[:,::1] spacetime, unsigned int Nr):
        """
          flow      : c object    : mathematical flow
          kernel    : c object    : interpolation and remapping kernel
          dist      : c object    : initial distribution
          spacetime : double[][3] : phase space-time axes
          Nr        : integer     : number of mass smoothing/redistribution for positiveness
        """
        self.flow = flow
        self.kernel = kernel
        self.dist   = dist
        self.spacetime = spacetime
        self.Nr = Nr
        self.Nd = flow.get_dimension()
        self.t = self.spacetime[self.Nd,0]
        self.threshold = self.dist.get_cutoff()
        print('Initializing dimensions..........', end='')
        self.initialize_dimensions()
        print('Ok')
        print('Initializing deformations........', end='')
        self.initialize_deformations()
        print('Ok')
        print('Initializing particles...........', end='')
        self.initialize_particles()
        print('Ok')
        print('Initializing transition matrix...', end='')
        self.transition_matrix = SparseMatrix(self.Np, self.Np)
        self.transition_matrix.set_identity()
        print('Ok')
        print('Running sanity check.............',end='')
        self.run_sanity_check()
        print('Ok')


    cdef void run_sanity_check(self) nogil:
        """ sanity check """
        if self.Nd != self.spacetime.shape[0]-1 :
            raise RuntimeError('The velocity field and spacetime=[x,t] dimensions do not match !')
        if self.Np != self.f.shape[0] :
            raise RuntimeError('The distribution array has not been properly initialized (wrong size) !')
        if not self.flow.is_stationary() and self.Np != self.strengths.shape[0] :
            raise RuntimeError('The strengths array has not been properly initialized (wrong size) !')


    cdef void initialize_dimensions(self) :
        """ Initialize the dimensions array and compute the total number of particles """
        cdef unsigned short i
        self.Np = 1
        self.dims = cvarray(shape=(self.Nd,), itemsize=sizeof(int), format="i")
        for i in range(self.Nd):
            self.dims[i] = <int> ( (self.spacetime[i,1] -  self.spacetime[i,0])/ self.spacetime[i,2] )
            self.Np *= self.dims[i]


    cdef void initialize_deformations(self):
        """ Initialize the distribution, strength and deformaion matrices """
        self.f = cvarray(shape=(self.Np,), itemsize=sizeof(double), format="d")
        if not self.flow.is_stationary():
            self.strengths = cvarray(shape=(self.Np,), itemsize=sizeof(double), format="d")
        self.P0DM = cvarray(shape=(self.Np, self.Nd+1), itemsize=sizeof(double), format="d")


    cdef void initialize_particles(self) nogil:
        """ Initialize the particle (coordinates, inener time step) """
        cdef unsigned long i
        for i in prange(self.Np, nogil=True):
            self.get_point_with_address(i, self.P0DM[i,0:self.Nd])
            self.P0DM[i, self.Nd] = 1.
            self.f[i] = self.dist.density(self.P0DM[i,0:self.Nd])
            self.f[i] = self.f[i] * (self.f[i] > self.threshold)


    cdef void reposition_particles(self) nogil :
        """ Snap the particle onto the grid """
        cdef unsigned long i
        for i in prange(self.Np, nogil=True):
            self.get_point_with_address(i, self.P0DM[i,0:self.Nd])
            self.P0DM[i, self.Nd] = 1.


    cdef long encode(self, int[] indices) nogil :
        """ Compute the row-major indexation of a Nd-array
            Input :
              indices : int[]   : sub-indices
            Output :
              address : integer : super index
        """
        cdef :
            unsigned short i
            unsigned long address = indices[0]
        for i in range(1, self.Nd): address = address * self.dims[i] + indices[i]
        return address


    cdef void decode(self, unsigned long address, int[] indices) nogil :
        """ Compute the sub-indices that compose an address
            Input :
              address : integer   : super-index
              indices : integer[] : sub-indices placeholder
        """
        cdef:
            unsigned short i
            unsigned long N = self.Np/self.dims[0]
        for i in range(1, self.Nd) :
            indices[i-1] = <int>( address / N )
            address = address - indices[i-1] * N
            N = N / self.dims[i]
        indices[self.Nd-1] = address


    cdef void get_point_with_address(self, unsigned long address, double[::1] point) nogil :
        """ Get the coordinates of point on the grid with its grid address
            Input :
              address : integer : super-index of a grid point
              point   : double[]: place holder for the point
        """
        cdef:
            unsigned short i
            unsigned long N = self.Np/self.dims[0]
            unsigned long indices
        for i in range(1, self.Nd) :
            indices = <int>( address / N )
            point[i-1] = self.spacetime[i-1,0] + self.spacetime[i-1,2] * indices
            address = address - indices * N
            N = N / self.dims[i]
        indices = <int>( address / N )
        point[self.Nd-1] = self.spacetime[self.Nd-1,0] + self.spacetime[self.Nd-1,2] * indices


    cdef void get_point_with_indices(self, int[] indices, double[::1] point) nogil :
        """ Get the coordinates of point on the grid with its indices
            Input :
              indices : int[] : sub-indices of a grid point
              point   : double[]: place holder for the point
        """
        cdef unsigned short i
        for i in range(self.Nd) : point[i] = self.spacetime[i,0] + self.spacetime[i,2] * indices[i]


    cdef void get_influence_zone(self, unsigned long address, int[:,::1] bounds) nogil :
        """ Determine the boundaries of the influence zone of a particle
            Input :
              address  : integer  : super-index of a particle
              bounds   : int[]    : placeholder for the bounds
        """
        cdef:
            unsigned int i
            unsigned short r = self.kernel.get_radius() + 1
            double anchor
        for i in range(self.Nd):
            anchor = (self.P0DM[address,i] - self.spacetime[i,0])/self.spacetime[i,2]
            bounds[i,0] = <int>( fmax(0, fmin(self.dims[i], anchor - r)) )
            bounds[i,1] = <int>( fmin(self.dims[i], fmax(0, anchor + r)) )


    cdef void advect_particles(self, double ti, double tf, double dt, short direction) nogil:
        """ Advect all the particles one step into the future
            Inputs:
              ti : double : departure time
              tf : double : arrival time
              dt : double : time step
              dir: intger : direction in which advection is done (-1: all direction/ 0: direction0, etc...)
        """
        cdef unsigned long i
        self.flow.set_mask(direction)
        for i in prange(self.Np, nogil=True): self.flow.advect0(self.P0DM[i,:],ti, self.P0DM[i,:], tf, dt)


    cdef void compute_sparse_entries(self, unsigned long col, unsigned short direction, SparseMatrix matrix) nogil:
        """ Compute the coefficients (row, col, weights) of the sparse transition table
            Inputs:
              col       : integer : column index
              direction : integer : splitting direction
        """


    cdef void compute_transition(self):
        """ Compute the transition matrices that contain the interpolation weights
            between the Lagrangian particles and the Eulerian grid
        """
        cdef :
            double dt = self.spacetime[self.Nd,2]
            double ti = self.t
            double tf
            double h
            unsigned long col
            unsigned short index
            unsigned short direction
            bint flow_stationary = self.flow.is_stationary()
            SparseMatrix matrix = SparseMatrix(self.Np, self.Np)
        #reserve enough memory for the sparse transition matrix when the field is stationary
        if flow_stationary:
            if self.splitting_stages==1 : matrix.reserve((2*self.kernel.get_radius()) ** self.Nd)
            else : matrix.reserve(2*self.kernel.get_radius())
        #if stationary field : compute the coefficients of the transition matrix
        #otherwise : compute the next step of the distribution
        for index in tqdm.tqdm(iterable=range(self.splitting_stages), desc='Directional advection :', ncols=100, disable=not flow_stationary):
            direction = self.splitting_directions[index]
            h = dt * self.splitting_scalers[index]
            tf = ti + h
            if (index!=0) or (index==0 and not flow_stationary) : self.reposition_particles()
            self.advect_particles(ti, tf, h, direction)
            #for col in prange(self.Np, nogil=True): self.compute_sparse_entries(col, direction, matrix)
            for col in range(self.Np): self.compute_sparse_entries(col, direction, matrix)
            if not flow_stationary and index<self.splitting_stages-1 : self.store_strengths()
            ti = tf
            if flow_stationary:
               matrix.assemble()
               self.transition_matrix.update(matrix)
               matrix.reset()
        #TODO : should delete the self.P0DM array if the flow_stationary


    cdef void distribute_strenghts(self) nogil:
        """ Compute the new values of the distribution f(x,t+dt) with the values strengths(x,t) """
        self.transition_matrix.dot(self.f, self.f)


    cdef void regularize_distribution(self) :
        """ Correct the (over/under)shoots with mass redistribution based upon
            a local or global a flux-corrected transport algorithm
        """


    cdef void store_strengths(self) nogil:
        """ Store the current distribution into the strengths then reset it """
        self.strengths[::1] = self.f[::1]
        self.f[::1] = 0.


    cpdef void update_distribution(self):
        """ Compute the values of f(X,t+dt) from the values of f(X,t) """
        if self.flow.is_stationary():
            if self.t==0: self.compute_transition()
            self.distribute_strenghts()
        else:
            self.store_strengths()
            self.compute_transition()
        #regularize the (over/under)-shoots
        for _ in range(self.Nr): self.regularize_distribution()
        self.t += self.spacetime[self.Nd,2]
