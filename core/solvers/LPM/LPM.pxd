#------------------------------------------------------------------------------#
# Forward Semi-Lagrangian Method Particles solver for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.flows.flow cimport Flow
from core.commons.kernels.kernel cimport Kernel
from core.commons.distributions.distribution cimport Distribution
from core.commons.maths.sparsematrix cimport SparseMatrix
from libcpp.vector cimport vector


cdef class LPM:

    cdef :
        Flow flow                 #mathematical flow function
        Kernel kernel             #inerpolation kernel
        Distribution dist         #phase space distribution
        double threshold          #threshold value below which a particle'strength is not used
        double[:,::1] spacetime   #space-time discretization parameters
        double[:,::1] P0DM        #zero order particle representation #position, velocity, relative volume, time step
        int[::1] dims             #arrays containing the number of grid points on each dimension
        unsigned short Nr         #number of local regularization
        unsigned short Nd         #number of dimensions
        unsigned long Np          #number of particles
        double[::1] f             #weights and distribution
        double[::1] strengths     #particles' strengths
        double t                  #current time
        vector[short] splitting_directions  #indices of splitting directions
        vector[double] splitting_scalers    #time step factor for each direction
        unsigned short splitting_stages     #number of splitting stages
        SparseMatrix transition_matrix  #placeholder for the sparse transition table

    cdef void run_sanity_check(self) nogil
    cdef void initialize_dimensions(self)
    cdef void initialize_deformations(self)
    cdef void initialize_particles(self) nogil
    cdef void reposition_particles(self) nogil
    cdef long encode(self, int[] indices) nogil
    cdef void get_point_with_address(self, unsigned long address, double[::1] point) nogil
    cdef void get_point_with_indices(self, int[] indices, double[::1] point) nogil
    cdef void decode(self, unsigned long address, int[] indices) nogil
    cdef void get_influence_zone(self, unsigned long address, int[:,::1] bounds) nogil
    cdef void advect_particles(self, double ti, double tf, double dt, short direction) nogil
    cdef void compute_sparse_entries(self, unsigned long col, unsigned short direction, SparseMatrix matrix) nogil
    cdef void compute_transition(self)
    cdef void distribute_strenghts(self) nogil
    cdef void regularize_distribution(self)
    cdef void store_strengths(self) nogil
    cpdef void update_distribution(self)
