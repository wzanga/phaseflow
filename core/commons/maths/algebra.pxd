#------------------------------------------------------------------------------#
# Custom linear algebra module headers
# year   : 2020
#	Author : Williams Zanga, Blanca Dalfo
#------------------------------------------------------------------------------#
#cython: language_level=3, boundscheck=False, nonecheck=False, wraparound=False, initializedcheck=False, cdivision=True
from libc.math cimport sqrt


cdef inline double vector_norm(double[::1] v) nogil:
    """ Compute the L2 norm of a vector
        Input :
            v : double[] : vector whose L2, norm we compute
     """
    cdef :
        double res=0
        unsigned int i
    for i in range(v.shape[0]): res += v[i]*v[i]
    return sqrt(res)


cdef inline double vector_relative_norm(double[::1] v1, double[::1] v2) nogil:
    """ Compute a relative error between two vectors
        Input :
            v1 : double[] : first vector
            v2 : double[] : second vector (reference)
    """
    cdef :
        double res=0
        unsigned int i
        unsigned int n = v1.shape[0]
    if n!= v2.shape[0]: raise ValueError("v1 (vector) and v2 (vector) must have compatible dimensions")
    for i in range(n):
        if v1[i]!=0 and v2[i]!=0:
            res+= ( (v1[i]-v2[i])*(v1[i]-v2[i]) )/( (v1[i]+v2[i])*(v1[i]+v2[i])/4 )
        if v1[i]!=0 and v2[i]==0:
            res+= (1-v2[i]/v1[i])*(1-v2[i]/v1[i])
        if v1[i]==0 and v2[i]!=0:
            res+= (1-v1[i]/v2[i])*(1-v1[i]/v2[i])
    return sqrt(res/n)


cdef inline void vector_scaling(double c, double[::1] v, double[::1] res) nogil:
    """ Element-wise multiplication of a vector by a scalar (res = c * v)
        Input :
            c   : double   : scalar
            v   : double[] : vector that we which to dilate
            res : double[] : placeholder for the dilated vector
    """
    cdef :
        unsigned int i
        unsigned int n = v.shape[0]
    if n!=res.shape[0]: raise ValueError("v (vector) and res (vector) must have the same length")
    for i in range(n): res[i] = c * v[i]


cdef inline void vector_combination(double c1, double[::1] v1, double c2, double[::1] v2, double[::1] res) nogil:
    """ Compute the linear combination of 2 vectors (res = c1 * v1 + c2 * v2)
        Input :
            c1  : double   : coefficient of v1
            v1  : double[] : first vector
            c2  : double   : coefficient of v2
            v2  : double[] : second vector
            res : double[] : placeholder for the resultant vector
    """
    cdef :
        unsigned int i
        unsigned int n = v1.shape[0]
    if n!=v2.shape[0]: raise ValueError("v1 (vector) and v2 (vector) must have the same length")
    if n!=res.shape[0]: raise ValueError("v1 (vector) and res (vector) must have the same length")
    for i in range(n): res[i] = c1 * v1[i] + c2 * v2[i]


cdef inline void vector_dot_matrix(double[::1] v, double[:,::1] M, double[::1] res) nogil:
    """ Compute (left-side) product of a vector by a matrix
        Input :
          v   : double[]    : first vector
          M   : double[][]  : list of sparse_coefficient
          res : double[]    : placeholder for the resultant vector
    """
    cdef :
        unsigned int i,j
        unsigned int dim0 = M.shape[0]
        unsigned int dim1 = M.shape[1]
    if dim0!= v.shape[0]	 : raise ValueError("M (matrix) and v (vector) must have compatible dimensions")
    if dim1!= res.shape[0] : raise ValueError("M (matrix) and res (vector) must have compatible dimensions")
    res[:] = 0
    for i in range(dim1):
        for j in range(dim0):
            res[i] +=  v[j] * M[i,j]


cdef inline void vector_division(double[::1] v1, double[::1] v2, double[::1] res) nogil :
    """ Hadamard pointwise vector division
        Input :
            v1 : double[] : first vector (numerator)
            v2 : double[] : second vector (denominator does not carry any zero)
            res: double[] : placeholder for the resultant vector
    """
    cdef :
        unsigned int i
        unsigned int n = v1.shape[0]
    if n!=v2.shape[0]: raise ValueError("v1 (vector) and v2 (vector) must have the same length")
    if n!=res.shape[0]: raise ValueError("v1 (vector) and res (vector) must have the same length")
    for i in range(n): res[i] = v1[i]/v2[i]


cdef inline void matrix_dot_vector(double[:,::1] M, double[::1] v, double[::1] res) nogil:
    """ Compute (right-side) product of a matri by a vector
        Input :
          M   : double[][]  : list of sparse_coefficient
          v   : double[]    : first vector
          res : double[]    : placeholder for the resultant vector
    """
    cdef :
        unsigned int i,j
        unsigned int dim0 = M.shape[0]
        unsigned int dim1 = M.shape[1]
    if dim0!= res.shape[0] : raise ValueError("M (matrix) and res (vector) must have compatible dimensions")
    if dim1!= v.shape[0]	 : raise ValueError("M (matrix) and v (vector) must have compatible dimensions")
    res[:] = 0
    for i in range(dim0):
        for j in range(dim1):
            res[i] += M[i,j] * v[j]


cdef inline void matrix_inverse2D(double[:,::1] M, double[:,::1] res) nogil:
    """ Compute the inverse of a (2x2) matrix
        Input:
          M   : double[][]  : matrix to inverse
          res : double[][]  : placeholder for the inverse
    """
    cdef double det = M[0,0]*M[1,1] - M[1,0]*M[0,1]
    if det == 0: raise ValueError("Non inversible 2D matrix")
    res[0,0] =  M[1,1] / det
    res[0,1] = -M[0,1] / det
    res[1,0] = -M[1,0] / det
    res[1,1] =  M[0,0] / det


cdef inline void matrix_inverse3D(double[:,::1] M, double[:,::1] res) nogil:
    """ Compute the inverse of a (3x3) matrix
        Input:
          M   : double[][]  : matrix to inverse
          res : double[][]  : placeholder for the inverse
    """
    # Extract the entries from the matrix
    cdef :
        double a = M[0,0]
        double b = M[0,1]
        double c = M[0,2]
        double d = M[1,0]
        double e = M[1,1]
        double f = M[1,2]
        double g = M[2,0]
        double h = M[2,1]
        double i = M[2,2]
    # Compute the determinant and check that it is not 0
    cdef double det = a*(e * i - f * h) + b*(-(d * i - f * g)) + c*(d * h - e * g)
    if (det == 0): raise ValueError("Determinant of matrix equals 0, no inverse exists")
    res[0,0] =  (e * i - f * h) / det
    res[1,0] = -(d * i - f * g) / det
    res[2,0] =  (d * h - e * g) / det
    res[0,1] = -(b * i - c * h) / det
    res[1,1] =  (a * i - c * g) / det
    res[2,1] = -(a * h - b * g) / det
    res[0,2] =  (b * f - c * e) / det
    res[1,2] = -(a * f - c * d) / det
    res[2,2] =  (a * e - b * d) / det


cdef inline void matrix_inverse4D(double[:,::1] M, double[:,::1] res) nogil:
    """ Compute the inverse of a (4x4) matrix
        Inputs:
          M   : double[][]  : matrix to inverse
          res : double[][]  : placeholder for the inverse
    """
    # Extract the entries from the matrix
    cdef :
        double a = M[0,0]
        double b = M[0,1]
        double c = M[0,2]
        double d = M[0,3]
        double e = M[1,0]
        double f = M[1,1]
        double g = M[1,2]
        double h = M[1,3]
        double i = M[2,0]
        double j = M[2,1]
        double k = M[2,2]
        double l = M[2,3]
        double m = M[3,0]
        double n = M[3,1]
        double o = M[3,2]
        double p = M[3,3]
    # Compute the determinant and check that it is not 0
    cdef :
        double M11 = f*(k*p - o*l) - g*(j*p - l*n) + h*(j*o - k*n)
        double M21 = b*(k*p - o*l) - c*(j*p - n*l) + d*(j*o - n*k)
        double M31 = b*(g*p - o*h) - c*(f*p - n*h) + d*(f*o - g*n)
        double M41 = b*(g*l - k*h) - c*(f*l - j*h) + d*(f*k - g*j)
        double det = a*M11 - e*M21 + i*M31 - m*M41
    if (det == 0): raise ValueError("Determinant of matrix equals 0, no inverse exists")
    res[0,0] = (-h*k*n + g*l*n + h*j*o - f*l*o - g*j*p + f*k*p) / det
    res[0,1] = (d*k*n - c*l*n - d*j*o + b*l*o + c*j*p - b*k*p) / det
    res[0,2] =  (-d*g*n + c*h*n + d*f*o - b*h*o - c*f*p + b*g*p) / det
    res[0,3] = (d*g*j - c*h*j - d*f*k + b*h*k + c*f*l - b*g*l) / det
    res[1,0] =  (h*k*m - g*l*m - h*i*o + e*l*o + g*i*p - e*k*p) / det
    res[1,1] = (-d*k*m + c*l*m + d*i*o - a*l*o - c*i*p + a*k*p) / det
    res[1,2] =  (d*g*m - c*h*m - d*e*o + a*h*o + c*e*p - a*g*p) / det
    res[1,3] = (-d*g*i + c*h*i + d*e*k - a*h*k - c*e*l + a*g*l) / det
    res[2,0] =  (-h*j*m + f*l*m + h*i*n - e*l*n - f*i*p + e*j*p) / det
    res[2,1] =  (d*j*m - b*l*m - d*i*n + a*l*n + b*i*p - a*j*p) / det
    res[2,2] = (-d*f*m + b*h*m + d*e*n - a*h*n - b*e*p + a*f*p) / det
    res[2,3] =  (d*f*i - b*h*i - d*e*j + a*h*j + b*e*l - a*f*l) / det
    res[3,0] = (g*j*m - f*k*m - g*i*n + e*k*n + f*i*o - e*j*o) / det
    res[3,1] =  (-c*j*m + b*k*m + c*i*n - a*k*n - b*i*o + a*j*o) / det
    res[3,2] = (c*f*m - b*g*m - c*e*n + a*g*n + b*e*o - a*f*o) / det
    res[3,3] =  (-c*f*i + b*g*i + c*e*j - a*g*j - b*e*k + a*f*k) / det


cdef inline void matrix_combination(double c1, double[:,::1] M1, double c2, double[:,::1] M2, double[:,::1] res) nogil:
    """ Compute the linear combination of 2 matrices (res = c1 * M1 + c2 * M2)
        Input :
          c1  : double      : coefficient of v1
          v1  : double[][]  : first matrix
          c2  : double      : coefficient of v2
          v2  : double[][]  : second matrix
          res : double[][]  : placeholder for the resultant matrix
    """
    cdef :
        unsigned int i, j
        unsigned int n = M1.shape[0]
        unsigned int m = M1.shape[1]
    if n!=M2.shape[0] or m!=M2.shape[1]: raise ValueError("M1 (matrix) and M2 (matrix) must have the same length")
    for i in range(n):
        for j in range(m):
            res[i,j] = c1 * M1[i,j] + c2 * M2[i,j]
