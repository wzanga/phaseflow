
#ifndef SparseMatrixWrapper_H
#include "Eigen/Sparse"
#include <mutex>
#define SparseMatrixWrapper_H

using namespace Eigen;
std::mutex locker;


template <class T, int S=Eigen::RowMajor> class SparseMatrixWrapper: public SparseMatrix<T, S> {

  private:
    std::vector< Eigen::Triplet<T> > triplets;

  public:
    /* Create a new SparseMatrixWrapper object */
    SparseMatrixWrapper<T, S>(): SparseMatrix<T, S>(){}

    /* Create a new SparseMatrixWrapper object with given dimensions */
    SparseMatrixWrapper<T, S>(unsigned long rows, unsigned long cols): SparseMatrix<T, S>(rows, cols){}

    /* Create a new SparseMatrixWrapper object by copying another */
    SparseMatrixWrapper<T, S>(const SparseMatrix<T, S> other): SparseMatrix<T, S>(other){}

    /* Destructor */
    ~SparseMatrixWrapper<T, S>(){ this->triplets.clear(); }

    /** Reset **/
    void reset(){
      this->setZero();
      this->triplets.clear();
    }

    /** Reserve memory for the triplets */
    void reserve_memory(unsigned long nnzeros){
      this->triplets.reserve(nnzeros);
    }

    /** Build the sparsematrix **/
    void assemble(){
      this->setFromTriplets(this->triplets.begin(), this->triplets.end());
      this->makeCompressed();
      this->triplets.clear();
    }

    /** Add a new coefficient : bounadry check must be done in cython extension class **/
    void insert(const long row, const long col, const T value){
      locker.lock();
      this->triplets.emplace_back(row,col,value);
      locker.unlock();
    }

    /* sparsematrix.sparsematrix  product */
    SparseMatrixWrapper<T, S> dot(const SparseMatrixWrapper& other) {
      return (SparseMatrixWrapper<T, S>)( ((*this) * other).pruned() );
    }

    /* sparsematrix.1Dvector product */
    void dot1d(T* v, T* result) {
      Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, 1> >vec(v, this->rows());
      Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, 1> >(result, this->rows()) = ((*this) * vec);
    }

    /* sparsematrix.2Dmatrix product */
    void dot2d(T* A, int numCols, T* result) {
      Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >mat(A, this->cols(), numCols);
      Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >(result, this->rows(), numCols) = ( (*this) * mat) ;
    }
};
#endif
