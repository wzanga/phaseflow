#------------------------------------------------------------------------------#
# Bessels expressions
# year   : 2020
#	Author : Blanca Dalfo
#------------------------------------------------------------------------------#
# cython: language_level=3, boundscheck=False, nonecheck=False, wraparound=False, initializedcheck=False, cdivision=True
from libc.math cimport pow, tgamma


cdef inline double modified_bessel_first_kind(double x, int alpha) nogil:
  """ Compute the modified bessel function
      formula from: https://en.wikipedia.org/wiki/Bessel_function#Modified_Bessel_functions:_I%CE%B1,_K%CE%B1
      Input :
        x     : double  : positive argument
        alpha : integer : order
      Output:
        I_alpha(x): double : modified bessel function of first kind and alpha order
  """
  cdef:
      double z = x / 2
      double z_squared = z*z
      double z_2m = 1
      double C = 1./tgamma(alpha+1)
      double sum = C * z_2m
      unsigned short m
  for m in range(6):
    C = 1./ ((m+1) * (m+alpha+1)) * C
    z_2m = z_squared * z_2m
    sum += C * z_2m
  return pow(z, alpha) * sum
