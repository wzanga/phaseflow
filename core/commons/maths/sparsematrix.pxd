#------------------------------------------------------------------------------#
# SparseMatrix definition file
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: language_level=3, boundscheck=False, nonecheck=False, wraparound=False, initializedcheck=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native -DEIGEN_NO_DEBUG

cdef extern from "sparsematrix.h" nogil :
  cdef cppclass SparseMatrixWrapper[double]:
      SparseMatrixWrapper(unsigned long nrows, unsigned long ncols) except +
      void setIdentity()
      void reset()
      void assemble()
      void reserve_memory(unsigned long nnzeros)
      void insert(unsigned long i, unsigned long j, double value)
      void dot1d(double* input, double* output)
      SparseMatrixWrapper dot(SparseMatrixWrapper& other)


cdef class SparseMatrix:
  
    cdef :
        SparseMatrixWrapper[double]* thisptr # hold a C++ instance
        readonly unsigned long nrows
        readonly unsigned long ncols

    cdef void set_identity(self) nogil
    cdef void reset(self) nogil
    cdef void assemble(self) nogil
    cdef void reserve(self, unsigned long nnzeros) nogil
    cdef void insert(self, unsigned long row, unsigned long col, double value) nogil
    cdef void dot(self, double[::1] u, double[::1] v) nogil
    cdef void update(self, SparseMatrix other) nogil
