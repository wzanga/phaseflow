#------------------------------------------------------------------------------#
# Flux limiters module headers
# year   : 2020
#	Author : Blanca Dalfo
#------------------------------------------------------------------------------#
#cython: language_level=3, boundscheck=False, wraparound=False, initializedcheck=False, cdivision=True
from libc.math cimport fmin, fmax, fabs


cdef inline double minmod(double r) nogil:
    """ Compute the minimod limiter
        Input :
          r : double : represents the ratio of successive gradients on the grid
    """
    res = fmax(0,fmin(1,r))
    return res


cdef inline double superbee(double r) nogil:
    """ Compute the superbee limiter
        Input :
          r : double : represents the ratio of successive gradients on the grid
    """
    res = fmax(0, fmin(2*r,1))
    res = fmax(res, fmin(r,2))
    return res


cdef inline double vanLeer(double r) nogil:
    """ Compute the van Leer limiter
        Input :
            r : double : represents the ratio of successive gradients on the grid
    """
    res = (r + fabs(r)) / (1 + fabs(r))
    return res


cdef inline double vanAlbada(double r) nogil:
    """ Compute the van Albada limiter
        Input :
            r : double : represents the ratio of successive gradients on the grid
    """
    res = (r*r + r) / (r*r + 1)
    return res
