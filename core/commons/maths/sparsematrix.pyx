#------------------------------------------------------------------------------#
# SparseMatrix definition file
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native -DEIGEN_NO_DEBUG


cdef class SparseMatrix:

    def __cinit__(self, unsigned long nrows, unsigned long ncols):
        """ Constructor
          Input
              nrows : integer : number of rows
              ncols : integer : nulber of columns
        """
        self.thisptr = new SparseMatrixWrapper(nrows, ncols)
        self.nrows = nrows
        self.ncols = ncols


    def __dealloc__(self):
        del self.thisptr


    cdef void set_identity(self) nogil:
        """ Initialize the sparse matrix as an identity matrix """
        self.thisptr[0].setIdentity()


    cdef void reset(self) nogil:
        """ reset the matrix and shrink the memory used """
        self.thisptr[0].reset()


    cdef void assemble(self) nogil:
        """ Assemble the sparse matrix from its triplet list """
        self.thisptr[0].assemble()


    cdef void reserve(self, unsigned long nnzeros) nogil:
        """ Reserve memory for the triplet list
            Input:
              nnzeros : integer : expected number of elements to store
        """
        self.thisptr[0].reserve_memory(nnzeros)


    cdef void insert(self, unsigned long row, unsigned long col, double value) nogil:
        """ Append a non zero coefficient to the matrix triplet list
            Input
                row : integer : row index
                col : integer : colum index
                value : double  : value @(row,col)
        """
        self.thisptr[0].insert(row, col, value)


    cdef void dot(self, double[::1] u, double[::1] v) nogil:
        """ Compute the dot product between the parse matric and a memoryview
            Input
                u : double[] : input vector placeholder
                v : double[] : output vector placeholder
        """
        self.thisptr[0].dot1d(<double*>(&u[0]), <double*>(&v[0]))


    cdef void update(self, SparseMatrix other) nogil:
        """ Update the transition matrix with the relation : T_new = other * T_old """
        self.thisptr[0] = other.thisptr[0].dot(self.thisptr[0])
