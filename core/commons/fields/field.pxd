#------------------------------------------------------------------------------#
# Header of the velocity field base class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#

cdef class Field:

    cdef :
        readonly bint stationary
        readonly bint driven
        readonly double[::1] mask
        readonly unsigned short Nd

    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil
    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil
    cdef double divU(self, double[::1] x, double t) nogil
    cdef void V(self, double[::1] s, double t, double[::1] ds) nogil
    cdef bint is_stationary(self) nogil
    cdef short get_dimension(self) nogil
    cdef void set_mask(self, short direction) nogil
    cdef void reset_mask(self) nogil
    cdef void characteristic(self, double[::1] xi, double ti, double[::] xf, double tf) nogil
