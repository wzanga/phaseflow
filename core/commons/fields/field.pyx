#------------------------------------------------------------------------------#
# Implementation of the velocity field base class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from cython.view cimport array as cvarray


cdef class Field:

    def __init__(self, bint stationary, unsigned int Nd):
        """ Constructor
            Inputs :
              stationary : boolean : indicate whether the velocity field is stationary
        """
        self.stationary = stationary
        self.driven = not stationary
        self.Nd = Nd
        self.mask = cvarray(shape=(self.Nd+1,), itemsize=sizeof(double), format="d")
        self.mask[::1] = 1.


    def __dealloc__(self):
        """ Destructor """


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluates the velocity field function @(x,t)
            Inputs :
              x : double[] : state vector
              t : double   : time point
              dx: double[] : placeholder for the output
        """


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t)
            Inputs :
              x : double[]  : state vector
              t : double    : time point
              Jx: double[][]: placeholder for the output (row-major indexation)
        """


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t)
          Inputs :
            x : double[] : state vector
            t : double   : time point
          Output :
              : double[] : value of the divergence
        """


    cdef void V(self, double[::1] s, double t, double[::1] ds) nogil:
        """ Evaluates the augmented velocity field function @(s=(x,volume),t)
            Inputs :
              s : double[] : augmented state vector s=(x,volume)
              t : double   : time point
              ds: double[] : placeholder for the output
        """
        cdef :
            unsigned short n = self.Nd
            unsigned short i
        self.U( s[:n-1], t, ds[:n-1] )
        ds[n] = self.divU( s[:n-1], t ) * s[n]
        for i in range(n+1) : ds[i] *= self.mask[i]


    cdef void characteristic(self, double[::1] xi, double ti, double[::] xf, double tf) nogil:
      """ Compute the analytical characteristic xf = X(tf; ti, xi) problem
          Inputs :
            xi : double[] : initial state
            ti : double   : initial time
            xf : double[] : final state placeholder
            tf : double   : final time
      """


    cdef bint is_stationary(self) nogil :
        """ Indicate whether or not U(x,t) has en explicit time dependence """
        return self.stationary


    cdef short get_dimension(self) nogil:
        """ Return the number of dimensions of the problem := size(X) """
        return self.Nd


    cdef void set_mask(self, short direction) nogil:
        """ Set all directional components of the mask to zero exception
            on the given direction
            Input :
              direction : integer : direction to set leave free
        """
        if direction >= 0:
            self.mask[::1] = 0.
            self.mask[self.Nd] = 1.
            self.mask[direction] = 1.


    cdef void reset_mask(self) nogil:
        """ Reset the mask to its isentropic state """
        self.mask[::1] = 1.
