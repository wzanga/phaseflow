#------------------------------------------------------------------------------#
# Implemetation of the Hump2D(x |mu,r0) distribution
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.distributions.distribution cimport Distribution
from libc.math cimport sin, cos, exp, M_PI
import numpy as np


cdef class Bump2D(Distribution):

    cdef readonly double rc


    def __init__(self, double[::1] m, double r):
        """ Constructor """
        super().__init__(m)
        self.rc = r
        self.min_value = 0
        self.max_value = 1


    cdef double density(self, double[::1] x) nogil:
        """ Evaluates the a distribution density function at a given point x
            Inputs:
              x : double[] : 2D vector where the distribution is evaluated
            Output:
              y : density(x)
        """
        cdef :
          double r2 = (x[0]-self.mean[0])**2 + (x[1]-self.mean[1])**2
          double r2c = self.rc * self.rc
        return exp(-r2c/(r2c - r2)) if r2 < r2c else 0


    cdef double[:,:] sample(self, ssize_t N):
        """ Samples N points from the density function
            Inputs:
              N : integer : number of points to sample
            Output:
              memoryview to the points
        """
        cdef :
          double[:,:] res = np.empty((N, 2), dtype=np.float64)
          double r,t
          ssize_t i
        for i in range(N):
          t = np.random.rand(1) * 2 * M_PI
          r = np.random.rand(1) * self.rc
          res[i,0] = self.mean[0] + r * cos(t)
          res[i,1] = self.mean[1] + r * sin(t)
        return res
