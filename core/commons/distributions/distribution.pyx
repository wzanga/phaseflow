#------------------------------------------------------------------------------#
# Abstract Distribution class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True


cdef class Distribution:

    def __init__(self, double[::1] mean):
        """ Constructor """
        self.mean = mean


    def __dealloc__(self):
        """ Destructor """


    cdef double density(self, double[::1] x) nogil:
        """ Evaluates the a distribution density function at a given point x
            Inputs:
              x : double[] : 2D vector where the distribution is evaluated
            Output:
              y : density(x)
        """
        return 0.


    cdef double[:,:] sample(self, ssize_t N):
        """ Samples N points from the density function
            Inputs:
              N : integer : number of points to sample
            Output:
              memoryview to the points
        """
        return None


    cdef double get_min(self) nogil:
        """ return the minimum value of the density """
        return self.min_value


    cdef double get_max(self) nogil:
        """ return the maximum value of the density """
        return self.max_value


    cdef double get_cutoff(self) nogil:
        """ return the maximum value of the density """
        return self.max_value * 1e-10
