#------------------------------------------------------------------------------#
# Implementation of the Zalesak2D slotted disc distribution
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.distributions.distribution cimport Distribution
from libc.math cimport fabs, sqrt
import numpy as np


cdef class Zalesak2D(Distribution):

    cdef :
        readonly double W
        readonly double H
        readonly double R
        

    def __init__(self, double[::1] m, double r, double w, double h):
        """ Constructor """
        super().__init__(m)
        self.W = r * w
        self.H = r * h
        self.R = r
        self.min_value = 0
        self.max_value = 1


    cdef double density(self, double[::1] x) nogil:
        """ Evaluates the a distribution density function at a given point x
            Inputs:
              x : double[] : 2D vector where the distribution is evaluated
            Output:
              y : density(x)
        """
        cdef :
          double dx0 = x[0]-self.mean[0]
          double dx1 = x[1]-self.mean[1]
        if (dx0**2 + dx1**2 <= self.R**2) and (fabs(dx0)>=0.5*self.W  or dx1>= self.H-self.R )  : return 1.
        return 0.


    cdef double[:,:] sample(self, ssize_t N):
        """ Samples N points from the density function
            Inputs:
              N : integer : number of points to sample
            Output:
              memoryview to the points
        """
        cdef :
          double[:,:] res = np.empty((N, 2), dtype=np.float64)
          double xc = self.mean[0], yc = self.mean[1]
          ssize_t i
        for i in range(N):
          res[i,0] = 2 * self.R * np.random.rand(1) +  (self.mean[0]-self.R)
          if fabs(res[i,0] - xc) < self.W/2 :
            res[i,1] = (2*self.R - self.H) * np.random.rand(1) + (yc + self.H - self.R)
          else:
            res[i,1] = sqrt( self.R**2 - (res[i,0]- xc)**2 )
            res[i,1] = 2 * res[i,1] * np.random.rand(1) + (yc - res[i,1])
        return res
