#------------------------------------------------------------------------------#
# Header for the abstract Distribution class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#

cdef class Distribution:

    cdef :
        readonly double min_value
        readonly double max_value
        readonly double[::1] mean

    cdef double density(self, double[::1] x) nogil
    cdef double[:,:] sample(self, ssize_t N)
    cdef double get_min(self) nogil
    cdef double get_max(self) nogil
    cdef double get_cutoff(self) nogil
