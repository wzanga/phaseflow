#------------------------------------------------------------------------------#
# Implementation of the Normal2D(x |mu, cov) distribution
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
#cython: language_level=3, boundscheck=False, wraparound=False, initializedcheck=False, cdivision=True
from core.commons.distributions.distribution cimport Distribution
cimport core.commons.maths.algebra as algebra
from cython.view cimport array as cvarray
from libc.math cimport exp, sqrt, M_PI
import numpy as np


cdef class Normal3D(Distribution):

    cdef :
        readonly double[:,::1] icov
        readonly double[:,::1] cov
        readonly double M0


    def __init__(self, double[::1] m, double[:,::1] cov):
        """ Constructor """
        super().__init__(m)
        self.cov = cov
        self.icov = cvarray(shape=(3,3), itemsize=sizeof(double), format="d")
        algebra.matrix_inverse3D(cov, self.icov)
        self.M0 = 1
        self.min_value = 0
        self.max_value = 1/self.M0
        return


    cdef double density(self, double[::1] x) nogil:
        """ Evaluates the a distribution density function at a given point x
            Inputs:
              x : double[] : 2D vector where the distribution is evaluated
            Output:
              y : density(x)
        """
        cdef :
            double dx0 = x[0]-self.mean[0]
            double dx1 = x[1]-self.mean[1]
            double dx2 = x[2]-self.mean[2]
            double res = 0
        res += dx0 * self.icov[0,0] * dx0
        res += dx0 * self.icov[0,1] * dx1
        res += dx0 * self.icov[0,2] * dx2
        res += dx1 * self.icov[1,0] * dx0
        res += dx1 * self.icov[1,1] * dx1
        res += dx1 * self.icov[1,2] * dx2
        res += dx2 * self.icov[2,0] * dx0
        res += dx2 * self.icov[2,1] * dx1
        res += dx2 * self.icov[2,2] * dx2
        res = 1./self.M0 * exp(-0.5 * res)
        return res


    cdef double[:,:] sample(self, ssize_t N):
        """ Samples N points from the density function
            Inputs:
              N : integer : number of points to sample
            Output:
              memoryview to the points
        """
        return np.random.multivariate_normal(self.mean,self.cov,N).astype(np.float64)
