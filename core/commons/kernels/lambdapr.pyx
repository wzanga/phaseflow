#------------------------------------------------------------------------------#
# Implementation of the Lambda_{p,r} kernels from
# "Couplage de modèles, algorithmes multi-échelles et calcul hybride", Jean-Matthieu Etancelin
#
# year   : 2020
#	Author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.kernels.kernel cimport Kernel
from libc.math cimport fabs


cdef class L21(Kernel):

    def __init__(self,):
        """ Constructor """
        super().__init__(moment=2,regularity=1,radius=2,degree=3)

    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
        cdef:
            double z = fabs(x)
            double z2 = z * z
            double z3 = z2 * z
            double output = 0
        if(z<1):
            output = 1. -5./2*z2 + 3./2*z3
        elif(1<=z and z<2):
            output = 2. - 4*z + 5./2*z2 - 1./2*z3
        return output



cdef class L42(Kernel):

    def __init__(self,):
        """ Constructor """
        super().__init__(moment=4,regularity=2,radius=3,degree=5)

    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
        cdef:
            double z = fabs(x)
            double z2 = z * z
            double z3 = z2 * z
            double z4 = z2 * z2
            double z5 = z4 * z
            double output = 0
        if(z<1):
            output = 1.-5./4*z2-35./12*z3+21./4*z4-25./12*z5
        elif(1<=z and z<2):
            output = -4. +75./4*z-245./8*z2+545./24*z3-63./8*z4+25./24*z5
        elif(2<=z and z<3):
            output = 18.-153./4*z+255./8*z2-313./24*z3+21./8*z4-5./24*z5
        return output



cdef class L84(Kernel):

    def __init__(self,):
        """ Constructor """
        super().__init__(moment=8,regularity=4,radius=5,degree=9)

    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
        cdef:
            double z = fabs(x)
            double z2 = z * z
            double z3 = z2 * z
            double z4 = z2 * z2
            double z5 = z4 * z
            double z6 = z3 * z3
            double z7 = z6* z
            double z8 = z4 * z4
            double z9 = z8 * z
            double output = 0
        if(z<1):
            output = 1. -205./144*z2 + 91./192*z4 -6181./320*z5 + 6337./96*z6 -2745./32*z7 + 28909./576*z8 - 3569./320*z9
        elif(1<=z and z<2):
            output = -154 +12757./12*z - 230123./72*z2 + 264481./48*z3 - 576499./96*z4 + 686147./160*z5 - 96277./48*z6 + 14221./24*z7 - 28909./288*z8 + 3569./480*z9
        elif(2<=z and z<3):
            output = 68776./7-1038011./28*z+31157515./504*z2-956669./16*z3+3548009./96*z4-2422263./160*z5+197255./48*z6-19959./28*z7+144545./2016*z8-3569./1120*z9
        elif(3<=z and z<4):
            output = -56375 +8314091./56*z-49901303./288*z2+3763529./32*z3-19648027./384*z4+9469163./640*z5-545977./192*z6+156927./448*z7-28909./1152*z8+3569./4480*z9
        elif(4<=z and z<5):
            output = 439375./7-64188125./504*z+231125375./2016*z2-17306975./288*z3+7761805./384*z4-2895587./640*z5+129391./192*z6-259715./4032*z7+28909./8064*z8-3569./40320*z9
        return output
