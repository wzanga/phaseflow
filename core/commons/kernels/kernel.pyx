#------------------------------------------------------------------------------#
# Dummy Implementation of the kernel base class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True


cdef class Kernel:

    def __init__(self, unsigned short moment, unsigned short regularity, unsigned short radius, unsigned short degree):
        """ Constructor """
        self.moment = moment
        self.regularity = regularity
        self.radius = radius
        self.degree = degree


    def __dealloc__(self):
        """ Destructor """


    cdef short get_radius(self) nogil :
        """ Return the radius of the kernel """
        return self.radius


    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
