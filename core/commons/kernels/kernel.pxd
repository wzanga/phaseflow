#------------------------------------------------------------------------------#
# Header of the kernel base class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
cdef class Kernel:
    cdef :
        readonly unsigned short moment
        readonly unsigned short regularity
        readonly unsigned short radius
        readonly unsigned short degree

    cdef short get_radius(self) nogil
    cdef double evaluate(self, double x) nogil
