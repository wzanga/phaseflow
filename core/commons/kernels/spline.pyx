#------------------------------------------------------------------------------#
# Implementation of the Bspline related kernels
#
# year   : 2020
#	Author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.kernels.kernel cimport Kernel
from libc.math cimport fabs, fmax, tgamma


cdef class Mspline(Kernel):
    cdef :
        unsigned short p
        double factorial_p_minus_one

    def __init__(self, unsigned short p):
        """ Constructor
            Input:
              p : integer : index of the spline kernel
        """
        if p%2 != 0 : raise RuntimeError(' Mspline index must be even !')
        super().__init__(moment=2,regularity=p-1,radius=p//2,degree=p-1)
        self.p = p
        self.factorial_p_minus_one = tgamma(p)

    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
        cdef:
          unsigned short i
          double z = fabs(x)
          double output = 0
          double c = 1./self.factorial_p_minus_one
        if 2*z<self.p:
            for i in range(self.p):
                output += c * fmax(0, z-i+self.p/2)**(self.p-1)
                c = - c * (self.p-i)/(i+1)
        return output



cdef class Wspline(Kernel):
    cdef :
        unsigned short p
        short shift

    def __init__(self, unsigned short p):
        """ Constructor
            Input:
              p : integer : index of the spline kernel
        """
        if p%2 != 0 : raise RuntimeError('Wspline index must be even !')
        super().__init__(moment=p,regularity=0,radius=p//2,degree=p-1)
        self.p = p
        self.shift = p//2 - 1

    cdef double evaluate(self, double x) nogil:
        """ Compute the kernel ponderation coefficient
            Input :
                x : double : input values
            Output: kernel(x)
        """
        cdef:
            short i
            double z = fabs(x)
            double output = 0
            short inf
            short sup
        if 2*z<self.p:
            inf = <short>(z) - self.shift
            sup = inf + 2*self.shift + 1
            output = - 1
            for i in range(inf, sup+1):
                if i!=0 : output *= (z/i - 1)
        return output
