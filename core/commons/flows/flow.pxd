#------------------------------------------------------------------------------#
# Header of the Flow base class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.fields.field cimport Field


cdef class Flow:

    cdef:
        Field field

    cdef void advect0(self, double[::1], double, double[::1], double, double) nogil
    cdef void advect1(self, double[::1] xi, double[:,::1] Ji, double ti, double[::1] xf, double[:,::1] Jf, double tf, double dt) nogil
    cdef bint is_stationary(self) nogil
    cdef short get_dimension(self) nogil
    cdef void set_mask(self, short direction) nogil
    cdef void reset_mask(self) nogil
