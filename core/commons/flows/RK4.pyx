#------------------------------------------------------------------------------#
# Runge-Kutta 4 flow
#
#  0   |    0.,    0.,     0.,     0.
#  1/2 |  1/2.,    0.,     0.,     0.
#  1/2 |    0.,  1/2.,     0.,     0.
#  1   |    0.,    0.,     1.,     0.
#  ----------------------------------
#      |   1/6,   1/3,    1/3,    1/6
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.maths.algebra cimport vector_combination
from core.commons.flows.flow cimport Flow
from core.commons.fields.field cimport Field
from cython.parallel import prange
from cython.view cimport array as cvarray
from libc.math cimport fmin


cdef class RK4(Flow):

    cdef:
        double ONE_6
        double ONE_2


    def __cinit__(self, Field f):
        """ Constructor """
        super().__init__(f)
        self.ONE_6 = 1/6.
        self.ONE_2 = 1/2.


    cdef void advect0(self, double[::1] xi, double ti, double[::1] xf, double tf, double dt) nogil :
        """ Advect a point along its characteristic trajectory i.e compute xf = X(tf; ti, xi)
            Inputs :
              xi : double[] : initial state
              ti : double   : initial time
              xf : double[] : final state placeholder
              tf : double   : final time
              dt : double   : pointer to the time step
        """
        xf[::1] = xi[::1]
        if(tf==ti): return
        cdef:
            unsigned short n = xi.shape[0]
            unsigned short i
            double t = ti
            double h = dt
            double[:,::1] K  #K=(xf, k1, k2, k3, k4)
        with gil:
            K = cvarray(shape=(5,n), itemsize=sizeof(double), format="d")

        while(t < tf):
            h = fmin(h,tf-t)
            K[0,:] = xf
            #compute k1
            self.field.V(xf, t       , K[1,:])
            #compute k2
            vector_combination(1., K[0,:], h * self.ONE_2, K[1,:], xf)
            self.field.V(xf, t+h/2.  , K[2,:])
            #compute k3
            vector_combination(1., K[0,:], h * self.ONE_2, K[2,:], xf)
            self.field.V(xf, t+h/2.  , K[3,:])
            #compute k4/
            vector_combination(1., K[0,:], h, K[3,:], xf)
            self.field.V(xf, t+h     , K[4,:])
            #compute the new value of xf
            for i in prange(n):
              xf[i] = K[0,i] + (h * self.ONE_6) * (K[1,i] + 2*K[2,i] + 2*K[3,i] + K[4,i])
            t+=h
        return
