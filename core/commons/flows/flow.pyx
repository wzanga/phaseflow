#------------------------------------------------------------------------------#
# Implementation of the flow class
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True


cdef class Flow:

    def __cinit__(self, Field f):
        """ Constructor """
        self.field = f


    def __dealloc__(self):
      """ Destructor """


    cdef void advect0(self, double[::1] xi, double ti, double[::1] xf, double tf, double dt) nogil:
        """ Advect a point along its characteristic trajectory i.e compute xf = X(tf; ti, xi)
            Inputs :
              xi : double[] : initial state
              ti : double   : initial time
              xf : double[] : final state placeholder
              tf : double   : final time
              dt : double   : pointer to the time step
        """


    cdef void advect1(self, double[::1] xi, double[:,::1] Ji, double ti, double[::1] xf, double[:,::1] Jf, double tf, double dt) nogil:
        """ Advect a point and its 1st order shape along its characteristic trajectory
            i.e compute xf = X(tf; ti, xi) and Jf = (dxf/dxi) * Ji
            Inputs :
              xi : double[]   : initial state
              Ji : double[][] : initial first order deformation matrix (row-major indextation)
              ti : double     : initial time
              xf : double[]   : final state placeholder
              Jf : double[][] : final first order deformation matrix placeholder (row-major indextation)
              tf : double     : final time
              dt : double     : pointer to the time step
        """


    cdef bint is_stationary(self) nogil:
        """ Indicate whether or not U(x,t) has en explicit time dependence """
        return self.field.is_stationary()


    cdef short get_dimension(self) nogil:
        """ return the number of dimensions of the problem := size(X) """
        return self.field.get_dimension()


    cdef void set_mask(self, short direction) nogil:
        """ set all directional components of the mask to zero exception
            on the given direction
            Input :
              direction : integer : direction to set leave free
        """
        self.field.set_mask(direction)


    cdef void reset_mask(self) nogil:
        """ Reset the mask to its isentropic state """
        self.field.reset_mask()
