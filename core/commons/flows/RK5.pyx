#------------------------------------------------------------------------------#
# Butcher's Runge-Kutta 5 flow
#
#  0   |    0.,    0.,     0.,     0.,     0.,    0.
#  1/4 |  1/4.,    0.,     0.,     0.,     0.,    0.
#  1/4 |  1/8.,  1/8.,     0.,     0.,     0.,    0.
#  1/2 |    0., -1/2.,     1.,     0.,     0.,    0.
#  3/4 |  3/16,    0.,     0.,  9/16.,     0.,    0.
#  1   |  -3/7,   2/7,  12/7., -12/7.,   8/7.,    0.
#  --------------------------------------------------
#      | 7/90.,    0., 32/90., 12/90., 32/90., 7/90.
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.maths.algebra cimport vector_dot_matrix
from core.commons.flows.flow cimport Flow
from core.commons.fields.field cimport Field
from cython.parallel import prange
from cython.view cimport array as cvarray
from libc.math cimport fmin


cdef class RK5(Flow):

    cdef :
        double ONE_90
        double ONE_7
        double ONE_2
        double ONE_4
        double ONE_8
        double ONE_16


    def __cinit__(self, Field f):
        """ Constructor """
        super().__init__(f)
        self.ONE_90 = 1/90.
        self.ONE_7  = 1/7.
        self.ONE_2  = 1/2.
        self.ONE_4  = 1/4.
        self.ONE_8  = 1/8.
        self.ONE_16 = 1/16.


    cdef void advect0(self, double[::1] xi, double ti, double[::1] xf, double tf, double dt) nogil :
        """ Advect a point along its characteristic trajectory i.e compute xf = X(tf; ti, xi)
            Inputs :
              xi : double[] : initial state
              ti : double   : initial time
              xf : double[] : final state placeholder
              tf : double   : final time
              dt : double   : pointer to the time step
        """
        xf[::1] = xi[::1]
        if(tf==ti): return
        cdef:
            unsigned short n = xi.shape[0]
            unsigned short i
            double t = ti
            double h = dt
            double[:,::1] K  #K=(xf, k1, k2, k3, k4, k5, k6)
            double[:,::1] C  #C=((1,B) , (1,A))
        with gil:
            K = cvarray(shape=(7,n), itemsize=sizeof(double), format="d")
            C = cvarray(shape=(5,7), itemsize=sizeof(double), format="d")

        K[:,::1]=0
        C[:,::1]=0
        C[:,0]=1
        C[0,1]=h  * self.ONE_4
        C[1,1]=h  * self.ONE_8
        C[1,2]=h  * self.ONE_8
        C[2,2]=-h * self.ONE_2
        C[2,3]=h
        C[3,1]=3*h    *self.ONE_16
        C[3,4]=9*h    *self.ONE_16
        C[4,1]=-3*h   *self.ONE_7
        C[4,2]=2*h    *self.ONE_7
        C[4,3]=12*h   *self.ONE_7
        C[4,4]=-12*h  *self.ONE_7
        C[4,5]=8*h    *self.ONE_7

        while(t < tf):
            h = fmin(h,tf-t)
            K[0,:] = xf
            #compute k1
            self.field.U(xf, t       , K[1,:])
            #compute k2
            vector_dot_matrix(C[0,:], K, xf)
            self.field.U(xf, t+h*self.ONE_4  , K[2,:])
            #compute k3
            vector_dot_matrix(C[1,:], K, xf)
            self.field.U(xf, t+h*self.ONE_4  , K[3,:])
            #compute k4
            vector_dot_matrix(C[2,:], K, xf)
            self.field.U(xf, t+h*self.ONE_2  , K[4,:])
            #compute k5
            vector_dot_matrix(C[3,:], K, xf)
            self.field.U(xf, t+3*h*self.ONE_4, K[5,:])
            #compute k6
            vector_dot_matrix(C[4,:], K, xf)
            self.field.U(xf, t+h     , K[6,:])
            #compute the new value of xf
            for i in prange(n):
              xf[i] = K[0,i] + (h * self.ONE_90) * (7*K[1,i] + 32*K[3,i] + 12*K[4,i] + 32*K[5,i] + 7*K[6,i])
            t+=h
        return
