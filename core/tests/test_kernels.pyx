#------------------------------------------------------------------------------#
# Unit-tests for the L21, L42 and L84 Kernels
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.kernels.kernel cimport Kernel
from core.commons.kernels import *
import unittest


class TestKernels(unittest.TestCase):

    def test_L21(self):
        cdef Kernel kernel = L21()
        x = [0., 0.5, 1., 1.5, 2., 2.5]
        result = [ kernel.evaluate(z) for z in x]
        target = [1.0, 0.5625, 0.0, -0.0625, 0.0, 0.0]
        map(lambda x, y: self.assertAlmostEqual(x,y), result, target)


    def test_L42(self):
        cdef Kernel kernel = L42()
        x = [0., 0.5, 1., 1.5, 2., 2.5, 3., 3.5]
        result = [ kernel.evaluate(z) for z in x]
        target = [1.0, 0.5859375, 0.0, -0.09765625, 0.0, 0.01171875, 0.0, 0.0]
        map(lambda x, y: self.assertAlmostEqual(x,y), result, target)


    def test_L84(self):
        cdef Kernel kernel = L84()
        x = [0., 0.5, 1., 1.5, 2., 2.5, 3., 3.5, 4., 4.5, 5., 5.5]
        result = [ kernel.evaluate(z) for z in x]
        target = [1.0, 0.6056213378906252, -6.554756737386924e-13, -0.13458251953198896, 9.390532795805484e-11, 0.034606933857503464, 2.928572939708829e-10, -0.0061798090318916366, 1.418811734765768e-10, 0.0005340579664334655, 0.0, 0.0]
        map(lambda x, y: self.assertAlmostEqual(x,y), result, target)


    def test_WP0(self):
        cdef Kernel kernel
        for p in range(2, 10, 2):
            kernel = Wspline(p)
            self.assertAlmostEqual(kernel.evaluate(0), 1)
            for i in range(1,2*p): self.assertAlmostEqual(kernel.evaluate(i), 0)
