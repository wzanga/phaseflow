#------------------------------------------------------------------------------#
# Unit-tests runner
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#

from core.tests.test_kernels import TestKernels
from core.tests.test_algebra import TestAlgebra
from core.tests.test_limiters import TestLimiters
from core.tests.test_bessel import TestBessel
import unittest

if __name__ == '__main__':
    unittest.main()
