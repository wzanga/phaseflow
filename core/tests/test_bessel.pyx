#------------------------------------------------------------------------------#
# Unit-tests for the bessels sub-library
# year   : 2020
# author : Blanca Dalfo
#------------------------------------------------------------------------------#
cimport core.commons.maths.bessel as bessel
import unittest

class TestBessel(unittest.TestCase):

    def test_modified_bessel_first_kind(self):
      # To calculate the value of the modified Bessel function of the first kind:
      # https://keisan.casio.com/exec/system/1180573475
      cdef double x = 1.5
      cdef int n = 0
      cdef double target = 1.64672319
      self.assertAlmostEqual(bessel.modified_bessel_first_kind(x,n), target)
