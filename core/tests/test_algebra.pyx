#------------------------------------------------------------------------------#
# Unit-tests for the algebra sub-library
# year   : 2020
# author : Blanca Dalfo
#------------------------------------------------------------------------------#
cimport core.commons.maths.algebra as algebra
import numpy as np
import unittest

class TestAlgebra(unittest.TestCase):
  
    def test_vector_norm(self):
      cdef double[::1] res = np.array([3,4], dtype=np.float64)
      cdef double target = 5
      self.assertEqual(algebra.vector_norm(res), target)


    def test_vector_relative_norm(self):
      cdef double[::1] v1 = np.array([40.114,-88.224], dtype=np.float64)
      cdef double[::1] v2 = np.array([40,-88], dtype=np.float64)
      cdef double target = 0.0026983585451066778
      self.assertEqual(algebra.vector_relative_norm(v1,v2), target)


    def test_vector_scaling(self):
      cdef double c = 2
      cdef double[::1] v = np.array([2,3], dtype=np.float64)
      cdef double[::1] res = np.zeros(v.shape[0] ,dtype=np.float64)
      cdef double[::1] target = np.array([4,6], dtype=np.float64)
      algebra.vector_scaling(c,v,res)
      cdef int i
      for i in range(res.shape[0]):
        self.assertEqual(res[i], target[i])


    def test_vector_combination(self):
      cdef double c1 = 2
      cdef double c2 = 3
      cdef double[::1] v1 = np.array([2,3], dtype=np.float64)
      cdef double[::1] v2 = np.array([1,4], dtype=np.float64)
      cdef double[::1] res = np.zeros(v1.shape[0] ,dtype=np.float64)
      cdef double[::1] target = np.array([7,18], dtype=np.float64)
      algebra.vector_combination(c1,v1,c2,v2,res)
      cdef int i
      for i in range(res.shape[0]):
        self.assertEqual(res[i], target[i])


    def test_vector_dot_matrix(self):
      cdef double[::1] v = np.array([1,2], dtype=np.float64)
      cdef double[:,::1] M = np.array([[1,0],[0,3]], dtype=np.float64)
      cdef double[::1] res = np.zeros(M.shape[1] ,dtype=np.float64)
      cdef double[::1] target = np.array([1,6], dtype=np.float64)
      algebra.vector_dot_matrix(v,M,res)
      cdef int i
      for i in range(res.shape[0]):
        self.assertEqual(res[i], target[i])


    def test_vector_division(self):
      cdef double[::1] v1 = np.array([2,8,3], dtype=np.float64)
      cdef double[::1] v2 = np.array([2,4,1], dtype=np.float64)
      cdef double[::1] res = np.zeros(v1.shape[0] ,dtype=np.float64)
      cdef double[::1] target = np.array([1,2,3], dtype=np.float64)
      algebra.vector_division(v1,v2,res)
      cdef int i
      for i in range(res.shape[0]):
        self.assertEqual(res[i], target[i])


    def test_matrix_dot_vector(self):
      cdef double[:,::1] M = np.array([[1,0,4],[0,3,2]], dtype=np.float64)
      cdef double[::1] v = np.array([1,2,3], dtype=np.float64)
      cdef double[::1] res = np.zeros(M.shape[0] ,dtype=np.float64)
      cdef double[::1] target = np.array([13,12], dtype=np.float64)
      algebra.matrix_dot_vector(M,v,res)
      cdef int i
      for i in range(res.shape[0]):
        self.assertEqual(res[i], target[i])


    def test_matrix_inverse3D(self):
      cdef double[:,::1] M = np.array([[1,2,3],[0,1,4],[5,6,0]], dtype=np.float64)
      cdef double[:,::1] target = np.array([[-24,18,5],[20,-15,-4],[-5,4,1]], dtype=np.float64)
      cdef double[:,::1] res = np.zeros((3,3),dtype=np.float64)
      algebra.matrix_inverse3D(M, res)
      cdef int i, j
      for i in range(res.shape[0]):
        for j in range(res.shape[1]):
          self.assertEqual(res[i,j], target[i,j])


    def test_matrix_inverse4D(self):
      cdef double[:,::1] M = np.array([[0,3,5,1],[5,1,3,5],[3,0,1,3],[1,3,5,2]], dtype=np.float64)
      cdef double[:,::1] target = np.array([[1,3,-4,-2],[-4,1,-3,4],[3,0,1,-3],[-2,-3,4,3]], dtype=np.float64)
      cdef double[:,::1] res = np.zeros((4,4),dtype=np.float64)
      algebra.matrix_inverse4D(M, res)
      cdef int i, j
      for i in range(res.shape[0]):
        for j in range(res.shape[1]):
          self.assertEqual(res[i,j], target[i,j])


    def test_matrix_combination(self):
      cdef double c1 = 3
      cdef double[:,::1] M1 = np.array([[1,2,3],[0,1,4]], dtype=np.float64)
      cdef double c2 = 2
      cdef double[:,::1] M2 = np.array([[0,2,1],[3,1,2]], dtype=np.float64)
      cdef double[:,::1] res = np.zeros((M1.shape[0],M1.shape[1]),dtype=np.float64)
      cdef double[:,::1] target = np.array([[3,10,11],[6,5,16]], dtype=np.float64)
      algebra.matrix_combination(c1,M1,c2,M2,res)
      cdef int i, j
      for i in range(res.shape[0]):
        for j in range(res.shape[1]):
          self.assertEqual(res[i,j], target[i,j])
