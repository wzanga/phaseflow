#------------------------------------------------------------------------------#
# Unit-tests for the limiters sub-library
# year   : 2020
# author : Blanca Dalfo
#------------------------------------------------------------------------------#
cimport core.commons.maths.limiters as limiters
import numpy as np
import unittest

class TestLimiters(unittest.TestCase):
    def test_minmod(self):
      cdef double r = 0.5
      cdef double target = 0.5
      self.assertEqual(limiters.minmod(r), target)


    def test_superbee(self):
      cdef double r = 0.5
      cdef double target = 1
      self.assertEqual(limiters.superbee(r), target)


    def test_vanLeer(self):
      cdef double r = -2
      cdef double target = 0
      self.assertEqual(limiters.vanLeer(r), target)


    def test_vanAlbada(self):
      cdef double r = 2
      cdef double target = 1.2
      self.assertEqual(limiters.vanAlbada(r), target)
