#------------------------------------------------------------------------------#
# Generic 2D model for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native
from core.solvers.LPM.LPM cimport LPM
from cython.view cimport array as cvarray
from cython.parallel import prange
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import tqdm
import os
import imageio
import glob


cdef class Model3D:

    cdef :
        LPM solver
        double[:,::1] center
        unsigned int Nt
        unsigned int Nd
        str savedir
        object labels

    def __cinit__(self, LPM solver, str savedir, str ID, object labels):
        """ Constructor
          Input:
            solver    : LPM         : PDE solver
            savedir   : string      : location of the folder containing the data
            ID        : string      : model ID (i.e name)
        """
        self.solver = solver
        self.Nd = self.solver.Nd
        self.Nt = <int> ((self.solver.spacetime[self.Nd,1] - self.solver.spacetime[self.Nd,0])/ self.solver.spacetime[self.Nd,2])
        self.savedir = savedir + ID
        self.labels = labels


    def monte_carlo(self, unsigned int N):
        """ Compute the characteristic on a set of N points sampled from the
            initial phase space density
            Input:
              N : integer : number of points sampled
        """
        cdef:
            double[:,::1] points = np.zeros( shape=(N, self.Nd+1), dtype=np.float64 )
            double dt = self.solver.spacetime[self.Nd,2]
            double ti = self.solver.t
            double tf = ti + dt
            unsigned int i
        with open(self.savedir + ".MonteCarlo.npy", 'wb') as f:
            self.solver.flow.reset_mask()
            points[:,:self.Nd] = self.solver.dist.sample(N) #(position, velocity, volume)
            np.save(f, points[:,:self.Nd])
            for _ in tqdm.tqdm(iterable=range(self.Nt), desc='Monte Carlo with {} points :'.format(N), ncols=100):
                for i in prange(N, nogil=True): self.solver.flow.advect0(points[i,:], ti, points[i,:], tf, dt)
                np.save(f, points[:,:self.Nd])
                ti , tf = tf, tf + dt


    def solve(self):
        """ Compute the phase space trajectory of the initial density """
        with open(self.savedir + ".Liouville.npy", 'wb') as f:
            np.save(f, self.solver.get_distribution() )
            for _ in tqdm.tqdm(iterable=range(self.Nt), desc="Solving Liouville's equation :", ncols=100):
                self.solver.update_distribution()
                np.save(f, self.solver.get_distribution() )


    def visualize(self, witness=True):
      cdef:
          double t0 = self.solver.spacetime[self.Nd,0]
          double tf = self.solver.spacetime[self.Nd,1]
          double dt = self.solver.spacetime[self.Nd,2]
          double x0min = self.solver.spacetime[0,0]
          double x0max = self.solver.spacetime[0,1]
          double x1min = self.solver.spacetime[1,0]
          double x1max = self.solver.spacetime[1,1]
          double x2min = self.solver.spacetime[2,0]
          double x2max = self.solver.spacetime[2,1]

      freader = open(self.savedir + ".Liouville.npy", 'rb')
      if witness : preader = open(self.savedir + ".MonteCarlo.npy", 'rb')
      #hepler plot function
      def plot3D(i, t, ax0, ax1, ax2, freader_shift=0, preader_shift=0):
          freader.seek(freader_shift*i)
          f = np.load(freader)
          cax = ax0.imshow( np.sum(f, axis = 2).T, cmap="BuPu", interpolation="none", origin='lower', extent=[x0min, x0max, x1min, x1max])
          cax = ax1.imshow( np.sum(f, axis = 1).T, cmap="BuPu", interpolation="none", origin='lower', extent=[x0min, x0max, x2min, x2max])
          cax = ax2.imshow( np.sum(f, axis = 0).T, cmap="BuPu", interpolation="none", origin='lower', extent=[x1min, x1max, x2min, x2max])
          if witness:
              preader.seek(preader_shift*i)
              points = np.load(preader)
              ax0.scatter(points[:,0], points[:,1], s=0.5, color="blue")
              ax1.scatter(points[:,0], points[:,2], s=0.5, color="blue")
              ax2.scatter(points[:,1], points[:,2], s=0.5, color="blue")

      fig, (ax0, ax1, ax2) = plt.subplots(nrows=1, ncols=3, figsize=(12, 6) )
      plot3D(0, t0, ax0, ax1, ax2)
      freader_shift = freader.tell()
      preader_shift = preader.tell() if witness else 0

      def update(val):
          t = stimes.val
          i = int((t-t0)/dt)
          ax0.images[0].remove()
          ax1.images[0].remove()
          ax2.images[0].remove()
          if witness :
              ax0.collections[0].remove()
              ax1.collections[0].remove()
              ax2.collections[0].remove()
          if t<=tf: plot3D(i, t, ax0, ax1, ax2, freader_shift, preader_shift)
          fig.canvas.draw_idle()

      axtimes = plt.axes(position=[0.2, 0.95, 0.5, 0.01], facecolor='lightgoldenrodyellow')
      stimes = Slider(axtimes,'Time', valmin=t0, valmax=tf, valinit=t0, valstep=dt)
      stimes.on_changed(update)
      ax0.set_xlabel(self.labels[0])
      ax0.set_ylabel(self.labels[1])
      ax1.set_xlabel(self.labels[0])
      ax1.set_ylabel(self.labels[2])
      ax2.set_xlabel(self.labels[1])
      ax2.set_ylabel(self.labels[2])
      ax0.set_xlim(x0min,x0max)
      ax0.set_ylim(x1min,x1max)
      ax1.set_xlim(x0min,x0max)
      ax1.set_ylim(x2min,x2max)
      ax2.set_xlim(x1min,x1max)
      ax2.set_ylim(x2min,x2max)
      plt.show()
      if witness : preader.close()
      freader.close()
      return
