#------------------------------------------------------------------------------#
# Generic 2D model for the propagation of f(x,t)
# abiding by the Liouville continuity equation : df/dt + div(uf) = 0
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
# distutils: language = c++
# distutils: extra_compile_args = -std=c++11 -O3 -ffast-math -march=native
from core.solvers.LPM.LPM cimport LPM
from cython.view cimport array as cvarray
from cython.parallel import prange
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import tqdm
import os
import imageio
import glob


cdef class Model2D:

    cdef :
        LPM solver
        double[:,::1] center
        unsigned int Nt
        unsigned int Nd
        str savedir
        object labels

    def __cinit__(self, LPM solver, str savedir, str ID, object labels):
        """ Constructor
          Input:
            solver    : LPM         : PDE solver
            savedir   : string      : location of the folder containing the data
            ID        : string      : model ID (i.e name)
        """
        self.solver = solver
        self.Nd = self.solver.Nd
        self.Nt = <int> ((self.solver.spacetime[self.Nd,1] - self.solver.spacetime[self.Nd,0])/ self.solver.spacetime[self.Nd,2])
        self.savedir = savedir + ID
        self.labels = labels


    def monte_carlo(self, unsigned int N):
        """ Compute the characteristic on a set of N points sampled from the
            initial phase space density
            Input:
              N : integer : number of points sampled
        """
        cdef:
            double[:,::1] points = np.zeros( shape=(N, self.Nd+1), dtype=np.float64 )
            double dt = self.solver.spacetime[self.Nd,2]
            double ti = self.solver.t
            double tf = ti + dt
            unsigned int i
        with open(self.savedir + ".MonteCarlo.npy", 'wb') as f:
            self.solver.flow.reset_mask()
            points[:,:self.Nd] = self.solver.dist.sample(N) #(position, velocity, volume)
            np.save(f, points[:,:self.Nd])
            for _ in tqdm.tqdm(iterable=range(self.Nt), desc='Monte Carlo with {} points :'.format(N), ncols=100):
                for i in prange(N, nogil=True): self.solver.flow.advect0(points[i,:], ti, points[i,:], tf, dt)
                np.save(f, points[:,:self.Nd])
                ti , tf = tf, tf + dt


    def solve(self):
        """ Compute the phase space trajectory of the initial density """
        with open(self.savedir + ".Liouville.npy", 'wb') as f:
            np.save(f, self.solver.get_distribution() )
            for _ in tqdm.tqdm(iterable=range(self.Nt), desc="Solving Liouville's equation :", ncols=100):
                self.solver.update_distribution()
                np.save(f, self.solver.get_distribution() )


    def analytical_solution(self):
        """ Compute the analytical phase space trajectory of the innitial density """
        cdef:
            double[:,::1] dist = np.zeros( (self.solver.dims[0], self.solver.dims[1]) , dtype=np.float64)
            double[::1] x = np.zeros(self.Nd, dtype=np.float64)
            double[::1] z = np.zeros(self.Nd, dtype=np.float64)
            double dt = self.solver.spacetime[self.Nd,2]
            double ti = self.solver.t
            int[2] I
        with open(self.savedir + ".Liouville.npy", 'wb') as f:
          for _ in tqdm.tqdm(iterable=range(self.Nt+1), desc="Analytically Solving Liouville's equation :", ncols=100):
              for I[0] in range(self.solver.dims[0]) :
                  x[0] = self.solver.spacetime[0,0] + I[0] * self.solver.spacetime[0,2]
                  for I[1] in range(self.solver.dims[1]):
                      x[1] = self.solver.spacetime[1,0] + I[1] * self.solver.spacetime[1,2]
                      self.solver.flow.field.characteristic(x, ti, z, 0)
                      dist[I[0], I[1]] = self.solver.dist.density(z)
              np.save(f, np.asarray(dist) )
              ti += dt


    cdef streamline(self, ax, x0min, x0max, x1min, x1max):
        """ Plot the velocity field streamline
            Input:
              ax : matplotlib axis objet :
              (x0min, x0max, x1min, x1max) : doubles : space boundaries
        """
        cdef :
            double[::1] s  = np.zeros(self.Nd+1, dtype=np.float64)
            double[::1] ds = np.zeros(self.Nd+1, dtype=np.float64)
            unsigned short N = 5
        X,Y = np.meshgrid( np.linspace(x0min,x0max,N) , np.linspace(x1min,x1max,N) )
        U,V = 0*X, 0*Y
        for i in range(N):
            for j in range(N):
                s[0] = X[i,j]
                s[1] = Y[i,j]
                self.solver.flow.field.U(s,0,ds)
                U[i,j] = ds[0]
                V[i,j] = ds[1]
        ax.streamplot(X, Y, U, V, linewidth=0.3, density=[0.5, 1], arrowsize=0.5, zorder=self.solver.flow.is_stationary(), color="orange")


    def visualize(self, bint witness=True, savegif=False):
        """ Display a 2D slid plot with the Liouville solution and possibly the Monte Carlo
            Input:
              witness : boolean : plot the Monte Carlo points ?
              savegif : boolean : generate a gif file out of the plot ?
        """
        cdef :
            double t0 = self.solver.spacetime[self.Nd,0]
            double tf = self.solver.spacetime[self.Nd,1]
            double dt = self.solver.spacetime[self.Nd,2]
            double x0min = self.solver.spacetime[0,0]
            double x0max = self.solver.spacetime[0,1]
            double x1min = self.solver.spacetime[1,0]
            double x1max = self.solver.spacetime[1,1]

        # Load the Liouville distribution (and Monte-Carlo points if requested)
        freader = open(self.savedir + ".Liouville.npy", 'rb')
        if(witness): preader = open(self.savedir + ".MonteCarlo.npy", 'rb')

        #hepler plot function
        def plot2D(i, t, ax, freader_shift=0, preader_shift=0):
            freader.seek(freader_shift*i)
            cax = ax.imshow( np.load(freader).T, cmap="BuPu", interpolation="none", origin='lower', extent=[x0min, x0max, x1min, x1max])
            if witness:
              preader.seek(preader_shift*i)
              points = np.load(preader)
              ax.scatter(points[:,0], points[:,1], s=0.5, color="blue")
            ax.set_title('Liouville phase space distribution f({},{},t) @t={} s'.format(self.labels[0],self.labels[1],str(round(t,3))) )
            return cax

        fig, ax = plt.subplots( figsize=((5*7)//4, 7) )
        ax.axhline(y=0, xmin=x0min, xmax=x0max, color='r', linewidth=0.5, linestyle='-.')
        ax.axvline(x=0, ymin=x1min, ymax=x1max, color='r', linewidth=0.5, linestyle='-.')
        self.streamline(ax, x0min, x0max, x1min, x1max)
        cbar = fig.colorbar(plot2D(0,t0,ax), ax=ax)
        freader_shift = freader.tell()
        preader_shift = preader.tell() if witness else 0

        def update(val):
            t = stimes.val
            i = int((t-t0)/dt)
            ax.images[0].remove()
            if witness : ax.collections[1].remove()
            if t<=tf: plot2D(i,t,ax, freader_shift, preader_shift)
            fig.canvas.draw_idle()
            if savegif : plt.savefig(self.savedir + str(i) + '.png')

        #Slider and plot parameters
        axtimes = plt.axes(position=[0.2, 0.95, 0.5, 0.01], facecolor='lightgoldenrodyellow')
        stimes = Slider(axtimes,'Time', valmin=t0, valmax=tf, valinit=t0, valstep=dt)
        stimes.on_changed(update)
        ax.set_xlim(x0min, x0max)
        ax.set_ylim(x1min, x1max)
        ax.set_xlabel(self.labels[0])
        ax.set_ylabel(self.labels[1])
        plt.show()
        #Save the images as a gif if requested
        if savegif:
          images = []
          listing = glob.glob(self.savedir + "*.png")
          listing.sort(key=os.path.getmtime)
          for f in listing:
            images.append( imageio.imread(f) )
            os.remove(f)
          imageio.mimsave(self.savedir + ".gif", images, duration=0.3)
        #close the readers and exit
        freader.close()
        preader.close()
        return
