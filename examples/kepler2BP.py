#------------------------------------------------------------------------------#
# Runner for the radial Kepler problem
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#------------------------------------------------------------------------------
side = 1
dx = 2e-2
dt = 0.1
x0 = (-side-1,side,dx)
x1 = (-side-0.5,side+0.5,dx)
t = (0, 2*np.pi, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 0.5e-2
mean =np.asarray([.5 , .0], dtype=np.float64)
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#------------------------------------------------------------------------------
#dist = Bump2D(mean,sig)
dist = Normal2D(mean,cov)
field = Kepler2BP(h=1, mu=1, e=0.2)
flow = RK5(field)
kernel = Wspline(6)
solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="kepler2BP", labels=['$x$','$y$'])
model.monte_carlo(100)
model.solve()
model.visualize()
