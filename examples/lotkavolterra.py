#------------------------------------------------------------------------------#
# Runner for the 2D Lotka-Voleterra predator/prey model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#------------------------------------------------------------------------------
side = 4
dx = 2e-2
dt = dx/side
x0 = (0,side,dx)
x1 = (0,side,dx)
t = (0, 2*np.pi,dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 1e-2
mean =np.asarray([0.4 , 0.6], dtype=np.float64)
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#------------------------------------------------------------------------------
#dist = Bump2D(mean,sig)
dist = Normal2D(mean,cov)
field = LotkaVolterra(1,1,1,1)
flow = RK4(field)
kernel = L84()
solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
#solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="lotkavolterra", labels=['$x_1$','$x_2$'])
model.monte_carlo(1000)
model.solve()
model.visualize()
