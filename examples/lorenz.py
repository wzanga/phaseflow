#------------------------------------------------------------------------------#
# Runner for the classical pendulum model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model3D import Model3D
from core.solvers.LPM.LPM3D import *
import numpy as np
import os
import tracemalloc

#------------------------------------------------------------------------------
side = 40
dx = 1
dt = 10e-2
x0 = (-side,side,dx)
x1 = (-side,side,dx)
x2 = (-side,side,dx)
t = (0, 4, dt)
spacetime = np.asarray([x0,x1,x2,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 5e-2 * 200
mean = np.asarray([10.,10.,10.], dtype=np.float64)
cov = np.asarray([ [sig,0,0],[0,sig,0],[0,0,sig] ], dtype=np.float64)
#-------------------------------------------------------------------------------
dist = Normal3D(mean,cov)
field = Lorenz(10., 25., 8/3.)
flow = RK4(field)
kernel = Wspline(6)
solver = LPM3DTensorial(flow, kernel, dist, spacetime, Nr=1)
#solver = LPM3DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model3D(solver, savedir=savedir, ID="lorenz", labels=['x','y','z'])
model.monte_carlo(100)
tracemalloc.start()
model.solve()
current, peak  = tracemalloc.get_traced_memory()
print("current: {} | peak:{}".format(current, peak))
tracemalloc.stop()
model.visualize()
