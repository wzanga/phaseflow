#------------------------------------------------------------------------------#
# Runner for the Van Der Pol oscillator
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#------------------------------------------------------------------------------
side = 3
dx = 2e-2
dt = 0.1
x0 = (-side,side,dx)
x1 = (-side,side,dx)
t = (0, 12, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 1e-1
mean = np.asarray([1. , 1.], dtype=np.float64)
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#-------------------------------------------------------------------------------
#dist = Bump2D(mean,sig*10)
dist = Normal2D(mean,cov)
field = VanDerPol(0.5)
flow = RK4(field)
kernel = Wspline(6)
solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
#solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="vanderpol", labels=['$x$','$v$'])
model.monte_carlo(500)
model.solve()
model.visualize(True)
