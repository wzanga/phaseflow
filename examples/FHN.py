#------------------------------------------------------------------------------#
# Runner for the classical pendulum model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#---------------------------------------------------------------
side = 2.25
dx = 1e-2
dt = 5e-2
x0 = (-side,side,dx)
x1 = (-1,2,dx)
t = (0, 10, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 5e-1
mean = np.asarray([0.5,0.5], dtype=np.float64)
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#-------------------------------------------------------------------------------
#dist = Bump2D(mean,sig)
dist = Normal2D(mean,cov * 0.05)
field = FHN(0.8,0.7,0.5,0.08)
flow = RK4(field)
kernel = Wspline(8)
#solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="pendulum", labels=['x','v'])
model.monte_carlo(1000)
model.solve()
model.visualize()
