#------------------------------------------------------------------------------#
# Runner for the fluid swirling model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#------------------------------------------------------------------------------
side = 2
dx = 2e-2
dt = dx/side
x0 = (0,side,dx)
x1 = (0,side,dx)
t = (0, 2*np.pi, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
mean = np.asarray([.5 , .75], dtype=np.float64)
#------------------------------------------------------------------------------
dist = Zalesak2D(mean,0.5, 0.5, 1.2)
field = Swirling(tau=0)
flow = RK4(field)
kernel = Wspline(6)
#solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver,savedir=savedir, ID="swirling", labels=['$x$','$y$'])
model.monte_carlo(1000)
model.solve()
model.visualize()
