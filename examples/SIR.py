#------------------------------------------------------------------------------#
# Runner for the classical pendulum model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model3D import Model3D
from core.solvers.LPM.LPM3D import *
import numpy as np
import os
import tracemalloc

#------------------------------------------------------------------------------
dx = 1.5e-2
dt = 5e-2
x0 = (-0.1,1,dx)
x1 = (-0.1,1,dx)
x2 = (-0.1,1,dx)
t = (0, 10, dt)
spacetime = np.asarray([x0,x1,x2,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 1e-3
mean = np.asarray([1./3,1./3,1./3], dtype=np.float64)
cov = np.asarray([ [sig,0,0],[0,sig,0],[0,0,sig] ], dtype=np.float64)
#-------------------------------------------------------------------------------
dist = Normal3D(mean,cov)
field = SIR(0.5, 0.035)
flow = RK4(field)
kernel = Wspline(4)
#solver = LPM3DTensorial(flow, kernel, dist, spacetime, Nr=1)
#solver = LPM3DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
solver = LPM3DSplitting(flow, kernel, dist, spacetime, 1, "StrangMarchuk")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model3D(solver, savedir=savedir, ID="lorenz", labels=['S','I','R'])
model.monte_carlo(100)
model.solve()
model.visualize()
