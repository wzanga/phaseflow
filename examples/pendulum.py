#------------------------------------------------------------------------------#
# Runner for the classical pendulum model
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os
#os.environ['KMP_DUPLICATE_LIB_OK']='True'
import tracemalloc


#------------------------------------------------------------------------------
side = 3
dx = 2e-2 * 1
dt = 5e-2
x0 = (-side,side,dx)
x1 = (-side,side,dx)
t = (0, 2*np.pi, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 5e-2
mean = np.asarray([0.6,0.], dtype=np.float64)*np.pi
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#-------------------------------------------------------------------------------
#dist = Bump2D(mean,sig*10)
dist = Normal2D(mean,cov)
field = Pendulum(0, 1., 0, 0)
flow = RK4(field)
kernel = Wspline(6)
#solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="pendulum", labels=['x','v'])
model.monte_carlo(100)
tracemalloc.start()
model.solve()
current, peak  = tracemalloc.get_traced_memory()
print("current: {} | peak:{}".format(current, peak))
tracemalloc.stop()
model.visualize()
