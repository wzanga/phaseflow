#------------------------------------------------------------------------------#
# Runner for the Duffing oscillator
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from core.commons.distributions import *
from core.commons.kernels import *
from core.commons.flows import *
from examples.fields import *
from core.models.model2D import Model2D
from core.solvers.LPM.LPM2D import *
import numpy as np
import os


#------------------------------------------------------------------------------
side = 5
dx = 5e-2
dt = dx/side
x0 = (-side,side,dx)
x1 = (-side,side,dx)
t = (0, 5*np.pi, dt)
spacetime = np.asarray([x0,x1,t], dtype=np.float64)
#------------------------------------------------------------------------------
sig = 5e-1
mean =np.asarray([1. , 3.], dtype=np.float64)
cov = np.asarray([ [sig,0],[0,sig] ], dtype=np.float64)
#------------------------------------------------------------------------------
dist = Bump2D(mean,sig)
#dist = Normal2D(mean,cov)
field = Duffing(alpha=1, beta=0.04, delta=0.1)
flow = RK4(field)
kernel = Wspline(10)
#solver = LPM2DTensorial(flow, kernel, dist, spacetime, Nr=1)
solver = LPM2DSplitting(flow, kernel, dist, spacetime, 1, "LieTrotter")
#------------------------------------------------------------------------------
savedir = os.path.dirname( os.path.abspath(__file__)) + "/data/"
model = Model2D(solver, savedir=savedir, ID="duffing", labels=['$x$','$v$'])
model.monte_carlo(100)
model.solve()
model.visualize()
