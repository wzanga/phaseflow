#------------------------------------------------------------------------------#
# Velocity field of a Lorenz system
# u(x,y,t) = [dx/dt,dy/dt]
#           dx/dt = x.(a1 - b1.y) - c1.x
#           dy/dt = y.(a2.x - b2) - c2.y
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field


cdef class SIR(Field):

    cdef :
        readonly double beta
        readonly double gamma


    def __init__(self, double beta, double gamma):
        super().__init__(True, 3)
        self.beta = beta
        self.gamma = gamma


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = - self.beta * x[1] * x[0]
        dx[1] = self.beta * x[1] * x[0] - self.gamma * x[1]
        dx[2] = self.gamma * x[1]


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = - self.beta * x[1]
        Jx[0,1] = - self.beta * x[0]
        Jx[0,2] = 0
        Jx[1,0] = self.beta * x[1]
        Jx[1,1] = self.beta * x[0] - self.gamma
        Jx[1,2] = 0
        Jx[2,0] = 0
        Jx[2,1] = self.gamma
        Jx[2,2] = 0


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return - self.beta * x[1]  + self.beta * x[0] - self.gamma
