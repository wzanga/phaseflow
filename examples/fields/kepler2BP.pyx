#------------------------------------------------------------------------------#
# Velocity field of 2body Kepler problem in angular formulation
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field
from libc.math cimport cos, sin


cdef class Kepler2BP(Field):

    cdef:
        readonly double h
        readonly double mu
        readonly double e


    def __init__(self, double h, double mu, double e):
        super().__init__(False, 2)
        self.h = h
        self.mu = mu
        self.e = e


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        cdef double s = sin(t)
        cdef double c = cos(t)
        cdef double r  = (self.h**2 / self.mu) / (1 + self.e * c)
        cdef double dr = (self.h**2 / self.mu) * (self.e * s) / (1 + self.e * c)**2
        dx[0] = dr * c - r * s
        dx[1] = dr * s + r * c


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return 0
