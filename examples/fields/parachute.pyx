#------------------------------------------------------------------------------#
# Velocity field of a balistic model with strong friction
# u(x,v,t) = [ v, -g - gamma * v^2]
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field
from libc.math cimport sqrt, log, tan, fabs


cdef class Parachute(Field):

    cdef :
        readonly double g      #gravtionatial acceleration (m/s^2)
        readonly double gamma  #uniform drag coefficient


    def __init__(self, double g, double gamma):
        super().__init__(True, 2)
        self.g = g
        self.gamma = gamma


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = x[1]
        dx[1] = -self.g - self.gamma * x[1] * x[1]


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = 0
        Jx[0,1] = 1
        Jx[1,0] = 0
        Jx[1,1] = - 2 * self.gamma * x[1]


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return - 2 * self.gamma * x[1]


    cdef void characteristic(self, double[::1] xi, double ti, double[::] xf, double tf) nogil:
        """ Compute the analytical characteristic xf = X(tf; ti, xi) problem """
        cdef:
            double deltaT = tf - ti
            double va
            double T
        if self.gamma == 0:
            xf[0] = xi[0] + xi[1]*deltaT - 0.5 * self.g * (deltaT**2)
            xf[1] = xi[1] - self.g * deltaT
        else:
            va = sqrt(self.g/self.gamma)
            T = tan(va * self.gamma * deltaT)
            xf[1] = va * (xi[1]-va*T) / (va + xi[1]*T)
            xf[0] = xi[0] + (1/self.gamma) * log( fabs(1 + xi[1]/va * T) / sqrt(1 + T**2) )
