#------------------------------------------------------------------------------#
# Velocity field of a simple Pendulum
# u(x,v,t) = [ v , - w^2 sin(x)  - bv +  a*cos(wt) ]
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field
from libc.math cimport cos, sin


cdef class Pendulum(Field):

    cdef:
        readonly double b   #damping
        readonly double w   #frequency
        readonly double af  #amplitude of forcing term
        readonly double wf  #frequency of forcing term


    def __init__(self, double b, double w, double af, double wf):
        if af==0 or wf==0:
          super().__init__(True, 2)
        else:
          super().__init__(False, 2)
        self.b = b
        self.w = w
        self.af = af
        self.wf = wf


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = x[1]
        dx[1] = -self.b * x[1] - self.w * self.w * sin(x[0]) + self.af * cos(self.wf * t)


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = 0
        Jx[0,1] = 1
        Jx[1,0] = -self.w * self.w * cos(x[0])
        Jx[1,1] = -self.b


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return -self.b
