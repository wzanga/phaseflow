#------------------------------------------------------------------------------#
# Velocity field of a Duffing Oscillator
# u(x,v,t) = [ v, -delta*v -alpha*x -beta*x^3]
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field


cdef class Duffing(Field):

    cdef :
        readonly double alpha
        readonly double beta
        readonly double delta


    def __init__(self, double alpha, double beta, double delta):
        super().__init__(True, 2)
        self.alpha = alpha
        self.beta = beta
        self.delta = delta


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = x[1]
        dx[1] = - self.alpha * x[0] - self.beta * x[0]**3 - self.delta * x[1]


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = 0
        Jx[0,1] = 1
        Jx[1,0] = - self.alpha - 3 * self.beta * x[0]**2
        Jx[1,1] = - self.delta


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return - self.delta
