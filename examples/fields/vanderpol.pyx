#------------------------------------------------------------------------------#
# Velocity field of a VanDerPol Oscillator
# u(x,v,t) = [ v, alpha*(1-x^2)v -x ]
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field


cdef class VanDerPol(Field):

    cdef readonly double alpha


    def __init__(self, double alpha):
        super().__init__(True, 2)
        self.alpha = alpha


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = x[1]
        dx[1] = x[1] * (1 - x[0]*x[0]) * self.alpha - x[0]


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = 0
        Jx[0,1] = 1
        Jx[1,0] = -2*x[0]*x[1]*self.alpha - 1
        Jx[1,1] = (1 - x[0]*x[0]) * self.alpha


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return (1 - x[0]*x[0]) * self.alpha
