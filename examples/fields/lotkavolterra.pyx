#------------------------------------------------------------------------------#
# Velocity field of a Lotka Voleterra model
# u(x,y,t) = [dx/dt,dy/dt]
#           dx/dt = x.(a1 - b1.y) - c1.x
#           dy/dt = y.(a2.x - b2) - c2.y
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field


cdef class LotkaVolterra(Field):

    cdef :
        readonly double a1
        readonly double b1
        readonly double a2
        readonly double b2


    def __init__(self, double a1, double b1, double a2, double b2):
        super().__init__(True, 2)
        self.a1 = a1
        self.b1 = b1
        self.a2 = a2
        self.b2 = b2


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        dx[0] = x[0] * (self.a1 - self.b1 * x[1])
        dx[1] = x[1] * (self.a2 * x[0] - self.b2)


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        Jx[0,0] = self.a1 - self.b1*x[1]
        Jx[0,1] = -self.b1*x[0]
        Jx[1,0] =  self.a2*x[1]
        Jx[1,1] = self.a2*x[0] - self.b2


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        return self.a1 - self.b1*x[1] + self.a2*x[0] - self.b2
