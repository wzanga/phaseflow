#------------------------------------------------------------------------------#
# Velocity field of a FitzHugh-Nagumo model (FHN)
# u(x,v,t) = [ x-x^3/3-v+Iext, x+a-bv ]
# Consists of two coupled, nonlinear ordinary differential equations
# -one describes the fast evolution of the neuronal membrane voltage
# -the other represents the slower “recovery” action of sodium channel
#deinactivation and potassium channel deactivation.
#
# year   : 2020
# author : Blanca Dalfo
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field


cdef class FHN(Field):

  cdef :
      readonly double I   #external stimulus
      readonly double a
      readonly double b
      readonly double phi


  def __init__(self, double a, double b, double I, double phi):
      super().__init__(True, 2)
      self.a = a
      self.b = b
      self.I = I
      self.phi = phi


  cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
      """ Evaluate the velocity field function @(x,t) """
      dx[0] = x[0] - x[0]*x[0]*x[0]/3 - x[1] + self.I
      dx[1] = (x[0] + self.a - self.b*x[1])*self.phi


  cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
      """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
      Jx[0,0] = 1 - x[0]*x[0]
      Jx[0,1] = -1
      Jx[1,0] = self.phi
      Jx[1,1] = -self.b * self.phi


  cdef double divU(self, double[::1] x, double t) nogil:
      """ Evaluate the divergence of the velocity field function @(x,t) """
      return -self.b*self.phi + 1 - x[0]*x[0]
