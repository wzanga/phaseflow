#------------------------------------------------------------------------------#
# Velocity field of a fluid swirling motion
#
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
# cython: boundscheck=False, nonecheck=False, wraparound=False, cdivision=True
from core.commons.fields.field cimport Field
from libc.math cimport cos, sin, M_PI


cdef class Swirling(Field):

    cdef readonly double tau


    def __init__(self, double tau):
        if tau == 0:
          super().__init__(True, 2)
        else:
          super().__init__(False, 2)
        self.tau = tau


    cdef void U(self, double[::1] x, double t, double[::1] dx) nogil:
        """ Evaluate the velocity field function @(x,t) """
        cdef double c = 1. if self.tau == 0 else cos(M_PI*t/self.tau)
        dx[0] =  c * (sin(0.5*M_PI*x[0])**2) * sin(M_PI*x[1])
        dx[1] = -c * (sin(0.5*M_PI*x[1])**2) * sin(M_PI*x[0])


    cdef void DU(self, double[::1] x, double t, double[:,::1] Jx) nogil:
        """ Evaluate the Jacobian matrix of the velocity field function @(x,t) """
        cdef double c = 1. if self.tau == 0 else cos(M_PI*t/self.tau)
        Jx[0,0] =   c * M_PI * cos(0.5*M_PI*x[0]) * sin(0.5*M_PI*x[0]) * sin(M_PI*x[1])
        Jx[0,1] =   c * M_PI * (sin(0.5*M_PI*x[0])**2) * cos(M_PI*x[1])
        Jx[1,0] = - c * M_PI * (sin(0.5*M_PI*x[1])**2) * cos(M_PI*x[0])
        Jx[1,1] = - c * M_PI * cos(0.5*M_PI*x[1]) * sin(0.5*M_PI*x[1]) * sin(M_PI*x[0])


    cdef double divU(self, double[::1] x, double t) nogil:
        """ Evaluate the divergence of the velocity field function @(x,t) """
        cdef double d0 =   cos(0.5*M_PI*x[0]) * sin(0.5*M_PI*x[0]) * sin(M_PI*x[1])
        cdef double d1 = - cos(0.5*M_PI*x[1]) * sin(0.5*M_PI*x[1]) * sin(M_PI*x[0])
        cdef double c = 1. if self.tau == 0 else cos(M_PI*t/self.tau)
        return c * M_PI * (d0+d1)
