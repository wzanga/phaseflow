#------------------------------------------------------------------------------#
# Cython compilation directives
# year   : 2020
# author : Williams Zanga
#------------------------------------------------------------------------------#
from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import numpy

sourcefiles = [ "./core/commons/maths/*.pyx",
                "./core/commons/flows/*.pyx",
                "./core/commons/kernels/*.pyx",
                "./core/commons/distributions/*.pyx",
                "./core/commons/fields/*.pyx",
                "./core/solvers/LPM/*.pyx",
                "./core/solvers/LPM/LPM2D/*.pyx",
                "./core/solvers/LPM/LPM3D/*.pyx",
                "./core/models/*.pyx",
                "./core/tests/*.pyx",
                "./examples/fields/*.pyx"
                ]

setup(name = 'PTCE', ext_modules = cythonize(sourcefiles), cmdclass = {"build_ext": build_ext}, include_dirs=[numpy.get_include()])
